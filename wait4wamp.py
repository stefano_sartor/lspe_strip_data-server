import os
import asyncio
from autobahn.asyncio.wamp import ApplicationSession, ApplicationRunner


class Component(ApplicationSession):
    async def onJoin(self, details):
        print('-- JOINED SESSION')
        self.leave()
        self.disconnect()
        #self.off()
        #asyncio.get_event_loop().stop()

    async def onLeave(self, details):
        print("-- SESSION LEFT")

    async def onDisconnect(self):
        print("-- DISCONNECTED")
        asyncio.get_event_loop().stop()



if __name__ == '__main__':
    url = os.environ.get('CBURL', u'ws://crossbar:8050/ws')
    realm = os.environ.get('CBREALM', u'realm1')

    runner = ApplicationRunner(url=url, realm=realm)
    runner.run(Component)
    print('++ WAIT COMPLETE')
