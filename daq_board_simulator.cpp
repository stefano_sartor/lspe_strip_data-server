#include "daq_pkt.hpp"
#include "strip_utils.hpp"
#include "slo_control.hpp"
#include "strip_singleton.hpp"
#include <thread>
#include <random>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <limits>
#include <atomic>
#include <thread>
#include <random>
#include "gps_pkt.hpp"

using namespace strip;
using boost::asio::ip::udp;


constexpr uint16_t POL_NO = 8;
uint32_t timestamp = 0;

uint32_t sinc_pkgs = 0;

class HkDaq {
    typedef std::list<addr_item> list;
    typedef std::list<addr_item>::iterator iterator;
public:
    typedef std::array<uint16_t,4> element;

    HkDaq() : pol_no(0){
        for(list& l : hk)
            l = singletons.config->address_hk_pol;

        current = hk[0].begin();

    }

    element next(uint16_t d1, uint16_t d2){
        if(current == hk[pol_no].end()){
            pol_no +=1;
            pol_no %= POL_NO;
            current = hk[pol_no].begin();
        }
        element e;
        e[0] = pol_no;
        e[1] = current->address;
        e[2] = current->val;
        current->val += d1;
        ++current;

        if(current != hk[pol_no].end()){
            e[3] = current->val;
            current->val += d2;
            ++current;
        }else{
            e[3] = 0;
        }
        return e;
    }

private:
   list hk[POL_NO];
   uint8_t pol_no;
   iterator current;
};

class GPSreceiver{
public:
  GPSreceiver(const boost::asio::ip::address& addr,uint16_t port);

private:
  void start_receive();

  void handle_receive(const boost::system::error_code& error,
      std::size_t bytes_transferred);

  boost::asio::ip::udp::socket _socket;
  boost::asio::ip::udp::endpoint _remote_endpoint;
  strip::GPSpkt _gps_pkt;

  char _buffer[GPSpkt::GPS_PKT_LEN];
};


GPSreceiver::GPSreceiver(const boost::asio::ip::address& addr,uint16_t port)
    : _socket(strip::utils::ioService::service, udp::endpoint(udp::v4(), port)),
     _remote_endpoint(addr,port)
{
    start_receive();
}

void GPSreceiver::start_receive(){
    _socket.async_receive_from(
                boost::asio::buffer(_buffer), _remote_endpoint,
                boost::bind(&GPSreceiver::handle_receive, this,
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred));
}

void GPSreceiver::handle_receive(const boost::system::error_code& error,
                                  std::size_t bytes_transferred){
    if (!error /*|| error == boost::asio::error::message_size*/){
        if(! _gps_pkt.decode(_buffer,_buffer+GPSpkt::GPS_PKT_LEN)){
            std::cout << "GPSreceiver: decode packet error, skip UDP packet" << std::endl;
            std::cout << _buffer << std::endl;
        }else if(bytes_transferred != GPSpkt::GPS_PKT_LEN){
          std::cout << "received: " << bytes_transferred <<" out of " << GPSpkt::GPS_PKT_LEN << std::endl;
        }else{
          timestamp = _gps_pkt.time_stamp*100;
          if((sinc_pkgs++ %300) == 0)
              std::cout << (int)_gps_pkt.time_stamp << " | "
                        << (int)_gps_pkt.day   << "/"
                        << (int)_gps_pkt.month << "/"
                        << (int)_gps_pkt.year << "  "
                        << (int)_gps_pkt.hour << ":"
                        << (int)_gps_pkt.minutes << ":"
                        << (int)_gps_pkt.seconds << "."
                        << (int)_gps_pkt.millis << std::endl;
        }
    }else{
       std::cout << "GPSreceiver:error while receiving UDP packet" << std::endl;
    }
    start_receive();
}
constexpr int th_num = 1;

void th_go(){
    strip::utils::ioService::service.run();
    std::cout << "----------------------------------ioService exiting" << std::endl;
}

int main(int argc, char const *argv[]) {

  try{
      strip::utils::parse_config("/etc/strip/config.json");
  }catch(const std::exception& e){
      std::cerr << e.what() <<std::endl;
      std::cerr << "EXIT due to a configuration error" << std::endl;
      return -1;
  }


  int64_t nano_sleep = 10000000;

  std::string server     = "localhost";
  std::string board_name = "";
  if(argc !=  3){
      std::cout << "usage: " << argv[0] << "<server_ip> <board name>" << std::endl;
      return -1;
  }else{
    server     = argv[1];
    board_name = argv[2];
  }

  const DaqConfig& conf = *singletons.config->daq_name.at(board_name);
  char board_id = conf.id;

  std::string gps_server = strip::singletons.config->gps.first;
  uint16_t    gps_port     = strip::singletons.config->gps.second;
  boost::asio::ip::address gps_addr = utils::resolve_or_fail(gps_server);
  GPSreceiver gps_rec(gps_addr,gps_port);

  std::vector<std::thread> th;
  for (auto i=0; i<th_num; i++)
    th.push_back(std::thread(th_go));

/*setup DAQ env */

  udp::socket   socket(strip::utils::ioService::service, udp::endpoint(strip::utils::resolve_or_fail(conf.sci_ip), conf.sci_port));
  udp::endpoint remote_endpoint(strip::utils::resolve_or_fail(server),conf.sci_port);


  uint8_t last_hk_addr = singletons.config->address_hk_pol.back().address;


  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_real_distribution<double> noise(0.0, 0.1);

  double_t ph = 0;
  double_t uph = 0;
  double_t p_step = 0.01;
  double_t u_step = 0.5;
  uint32_t last_timestamp = 0;


  HkDaq hk_pkts;


  DaqUdpPkt pkts;
  for(auto& p : pkts.pkts)
          p.board_id = board_id;

  int pkt_count = 0;


/*END setup DAQ env*/


  while(timestamp == 0); //busywait the GPS packet
  std::cout << "GPS SYNCHRO DONE" << std::endl;

/* DATA LOOP*/
  while(true){
      auto t_begin = std::chrono::system_clock::now();

      if((pkt_count %30000) == 0)
          std::cout<< std::setw(5) << std::setfill('0') << timestamp << " TOT:" << pkt_count << std::endl;

      for(size_t off=0; off < strip::DAQ_UDP_PKTS; off++){
          auto t_begin = std::chrono::system_clock::now();
          DaqPkt& p = pkts.pkts[off];
          p.time_stamp = timestamp++;
          p.phb = p.time_stamp%2;

          int i = 1;
          for(auto j =0; j < NUM_POLS_PER_BOARD; j++){
              p.dem_Q1(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
              p.pwr_Q1(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
              i++;

              p.dem_U1(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
              p.pwr_U1(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
              i++;

              p.dem_U2(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
              p.pwr_U2(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
              i++;

              p.dem_Q2(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
              p.pwr_Q2(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
              i++;
          }
          HkDaq::element hk = hk_pkts.next(noise(mt)*100,noise(mt)*100);

          /*if(p.board_id=='0'){
              std::cout << hk[0] << std::hex << "[" << hk[1] << "]" << std::dec << std::endl;
          }*/

          p.pol_no       = hk[0];
          p.base_address = hk[1];
          p.hk_1         = hk[2];
          p.hk_2         = hk[3];

          p.encode(&pkts.buffer[off * DAQ_PKT_LEN],&pkts.buffer[(off+1) * DAQ_PKT_LEN]);

          pkt_count++;
          ph += p_step;
          uph += u_step;

          auto stime = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now() - t_begin);
          std::this_thread::sleep_for(std::chrono::nanoseconds(nano_sleep - stime.count()));
      }
      socket.send_to(boost::asio::buffer(pkts.buffer),remote_endpoint);




  }


  for(auto& t : th)
    t.join();
  return 0;
}
