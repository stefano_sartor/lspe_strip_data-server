#include "include/json/utils.hpp"
#include <iostream>


namespace js = rapidjson;

int main(int argc,char* argv[]){
    if(argc != 2){
        std::cout << "usage: " << argv[0] << " json_text" << std::endl;
        return -1;
    }

    std::string json_s = argv[1];

    js::Document d;
    d.Parse<0>(json_s.c_str() ,json_s.size());

    if(d.HasParseError()){
        std::cout << "INPUT STRING ERROR"<< std::endl;
        return 1;
    }


    msgpack::type::variant v = load(d);
    std::string s = serialize(v);

    std::cout << s << std::endl;
    return 0;
}