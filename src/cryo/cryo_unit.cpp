#include "../../include/cryo/cryo_unit.hpp"


void CryoUnit::set_config(const boost::property_tree::ptree &pt)
{
    _commands.clear();
    for (auto &item : pt.get_child("commands"))
    {
        auto &cmd = _commands[item.first];
        for (auto &in : item.second.get_child("args"))
        {
            cmd.first.push_back(in.second.get_value<std::string>());
        }
        for (auto &out : item.second.get_child("ret"))
        {
            cmd.second.push_back(out.second.get_value<std::string>());
        }
    }
}