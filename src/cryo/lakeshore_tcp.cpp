#include "../../include/cryo/lakeshore_tcp.hpp"
#include "../../include/strip/utils.hpp"
#include <iostream>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>

using boost::asio::ip::tcp;

LakeshoreTCP::LakeshoreTCP() : _socket(strip::utils::ioService::service) {}

std::string escaped(const std::string &input)
{
    std::string output;
    output.reserve(input.size());
    for (const char c : input)
    {
        switch (c)
        {
        case '\a':
            output += "\\a";
            break;
        case '\b':
            output += "\\b";
            break;
        case '\f':
            output += "\\f";
            break;
        case '\n':
            output += "\\n";
            break;
        case '\r':
            output += "\\r";
            break;
        case '\t':
            output += "\\t";
            break;
        case '\v':
            output += "\\v";
            break;
        default:
            output += c;
            break;
        }
    }

    return output;
}

void LakeshoreTCP::set_config(const boost::property_tree::ptree &pt)
{
    Lakeshore::set_config(pt); // set the commands
    _ip = pt.get<std::string>("connection.ip");
    _port = pt.get<uint16_t>("connection.port");
}

void LakeshoreTCP::do_send(const std::stringstream &msg, boost::system::error_code &ec)
{
    ec = boost::system::error_code(boost::system::errc::success, boost::system::system_category());
    if (!_socket.is_open())
    {
        connect(ec);
        if (ec)
            return;
    }
    std::cout << "LadkeshoreTCP send: \"" << escaped(msg.str()) << "\"" << std::endl;
    _socket.write_some(boost::asio::buffer(msg.str()), ec);
}

std::stringstream LakeshoreTCP::do_receive(uint64_t ms_timeout, boost::system::error_code &ec)
{
    std::stringstream ss;
    ec = boost::system::error_code(boost::system::errc::success, boost::system::system_category());
    if (!_socket.is_open())
    {
        connect(ec);
        if (ec)
            return ss;
    }

    memset(_buffer, '\0', BUFFER_SIZE);

    auto s = _socket.native_handle();
    fd_set set;
    struct timeval timeout;
    FD_ZERO(&set);   /* clear the set */
    FD_SET(s, &set); /* add our file descriptor to the set */
    timeout.tv_sec = ms_timeout / 1000;
    timeout.tv_usec = (ms_timeout % 1000) * 1000;
    int rv = select(s + 1, &set, NULL, NULL, &timeout);
    switch (rv)
    {
    case -1:
        ec = boost::system::errc::make_error_code(boost::system::errc::io_error);
        break;
    case 0:
        ec = boost::system::errc::make_error_code(boost::system::errc::timed_out);
        break;
    default:
        _socket.read_some(boost::asio::buffer(_buffer, BUFFER_SIZE), ec);
        ss << _buffer;
        std::cout << "LadkeshoreTCP recv: \"" << escaped(ss.str()) << "\"" << std::endl;
        break;
    }

    return ss;
}

void LakeshoreTCP::connect(boost::system::error_code &ec)
{
    ec = boost::system::error_code(boost::system::errc::success, boost::system::system_category());
    tcp::endpoint ep(strip::utils::resolve_or_fail(_ip), _port);
    boost::asio::connect(_socket, &ep, ec);
}

void LakeshoreTCP::start(boost::system::error_code &ec)
{
    connect(ec);
}

void LakeshoreTCP::stop(boost::system::error_code &ec)
{
    _socket.close(ec);
}