#include "../../include/cryo/cryo_stream.hpp"
#include "../../include/strip/singleton.hpp"
#include <chrono>

namespace strip {
CryoStream::CryoStream(uint32_t multiplexer_hysteresis)
    : _multiplexer_hysteresis(multiplexer_hysteresis),
      _cryo_pkt_sig(singletons.workers_pool) {}

void CryoStream::connect(signal_timer &sig) {
  _conn = sig.connect(std::bind(&CryoStream::slot_collect_cryo, this));
}

void CryoStream::slot_collect_cryo() {
  typedef std::map<msgpack::type::variant, msgpack::type::variant> mpmap;

  for (const auto &sw : singletons.cryo_probes) {
    std::string multi_str = "";
    if (sw.first == "0")
      multi_str = "ALL0";
    else if (sw.first == "1")
      multi_str = "ALL1";
    if (!multi_str.empty()) {
      try {
        mpmap cmd;
        cmd["command"] = multi_str;
        cmd["args"] = std::vector<msgpack::type::variant>();
        auto ret = singletons.cryo_units.at("Multiplexer")->send_command(cmd);
        std::this_thread::sleep_for(
            std::chrono::milliseconds(_multiplexer_hysteresis));
        auto &map_ret = ret.as_map();
        if (map_ret["status"].as_string() != "OK") {
          utils::Log::log(utils::LogMessage::Level::ERROR, "Multiplexer",
                          "status returned " + map_ret["error"].as_string());
          continue;
        }
      } catch (const std::exception &e) {
        utils::Log::log(utils::LogMessage::Level::ERROR, "Multiplexer",
                        "exception " + std::string(e.what()));
        continue;
      }
    }
    for (const auto &ch : sw.second) {
      try {
        mpmap cmd;
        cmd["command"] = "SRDG?";
        cmd["args"] = std::vector<msgpack::type::variant>();
        cmd["args"].as_vector().push_back(ch.first);
        auto ret =
            singletons.cryo_units.at(ch.second->cryo_unit())->send_command(cmd);
        auto &map_ret = ret.as_map();
        if (map_ret["status"].as_string() == "OK") {
          map_ret["mjd"] = utils::Time::get_now_MJD();
          map_ret["id"] = ch.second->id();
          map_ret["calibrated"] = ch.second->calibrate(
              map_ret.at("ret").as_vector().front().as_double());
          std::shared_ptr<msgpack::type::variant> pkt_ptr(
              new msgpack::type::variant());
          *pkt_ptr = ret;
          _cryo_pkt_sig(pkt_ptr);
        } else {
          utils::Log::log(utils::LogMessage::Level::ERROR, ch.second->cryo_unit(),
                          "status returned " + map_ret["error"].as_string());
        }
      } catch (const std::exception &e) {
        utils::Log::log(utils::LogMessage::Level::ERROR, ch.second->cryo_unit(),
                        "exception " + std::string(e.what()));
      }
    }
  }
}
} // namespace strip
