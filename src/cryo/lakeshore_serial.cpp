#include "../../include/cryo/lakeshore_serial.hpp"

void LakeshoreSERIAL::set_config(const boost::property_tree::ptree &pt)
{
    Lakeshore::set_config(pt); // set the commands
}

void LakeshoreSERIAL::do_send(const std::stringstream &msg, boost::system::error_code& ec)
{
}

std::stringstream LakeshoreSERIAL::do_receive(uint64_t ms_timeout, boost::system::error_code& ec)
{
}