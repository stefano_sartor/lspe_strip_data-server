#include "../../include/db/slo_command.hpp"
#include "../../include/db/slo_command_data.hpp"
#include "../../odb/slo_command_data-odb.hxx"
#include <sstream>
#include <iomanip>
#include <odb/transaction.hxx>
#include <chrono>
#include "../../include/strip/utils.hpp"

namespace strip {

const std::string DbSloCommand::class_name = "DbSloCommand";

DbSloCommand::DbSloCommand(const std::string& user, const std::string& password, const std::string& db_name, const std::string &host, uint16_t port){
    _db.reset(new odb::mysql::database (user,password,db_name,host,port));
}

void DbSloCommand::connect_req(signal_slo_req &sig){
    signal_slo_req::Connection conn = sig.connect(std::bind(&DbSloCommand::slot_req, std::ref(*this),std::placeholders::_1));
    _conn_req.push_back(conn);
}

void DbSloCommand::connect_ack(signal_slo_req &sig){
    signal_slo_req::Connection conn = sig.connect(std::bind(&DbSloCommand::slot_ack, std::ref(*this),std::placeholders::_1));
    _conn_ack.push_back(conn);
}

void DbSloCommand::persist(std::shared_ptr<const SloCommand> &req, bool is_ack){
    DbSloRequest d;
    d.board_name = req->board_name;
    d.pol_no = req->pol_no;
    d.method = req->method;
    d.type = req->type;
    d.base_addr = req->base_addr;
    d.counter = req->counter;
    d.user = req->user;
    d.message = req->message;
    d.unix_nano = std::chrono::duration_cast<std::chrono::nanoseconds>(req->timestamp.time_since_epoch()).count();
    d.mjd = utils::Time::get_unix_MJD(d.unix_nano);

    std::stringstream ss;
    ss << "[" << std::hex ;
    for(auto i=0; i<req->data.size()-1; i++)
        ss << req->data[i] << ",";
    ss << req->data.back() << "]";

    d.data = ss.str();


    if(is_ack){
        d.res.reset(new strip::SloCommand::Result(req->res));
        d.req_ack = DbSloRequest::Ack::ACK;
    }else{
        d.req_ack = DbSloRequest::Ack::REQ;
    }

    try{
    odb::transaction t (_db->begin ());
    _db->persist(d);
    t.commit();
    }catch(const odb::exception& e){
        utils::Log::log(utils::LogMessage::ERROR,class_name,e.what());
    }
}

}
