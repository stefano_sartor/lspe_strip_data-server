#include "../../include/db/log.hpp"
#include "../../include/db/log_data.hpp"
#include "../../odb/log_data-odb.hxx"

#include <odb/transaction.hxx>

#include "../../include/strip/utils.hpp"
#include <chrono>


namespace strip {
const std::string DbLog::class_name = "DbLog";

DbLog::DbLog(const std::string &user, const std::string &password, const std::string &db_name, const std::string& host, uint16_t port){
    _db.reset(new odb::mysql::database (user,password,db_name,host,port));
}

void DbLog::connect_log(utils::signal_log &sig){
    _conn_log = sig.connect(std::bind(&DbLog::slot_log, std::ref(*this),std::placeholders::_1));
}

void DbLog::slot_log(std::shared_ptr<const utils::LogMessage> msg){
    if (msg->emit_class == class_name) // prevent log loop
        return;

    DbLogMessage l;
    l.emit_class = msg->emit_class;
    l.level = msg->level;
    l.message = msg->message;
    l.user = msg->user;
    l.unix_nano = std::chrono::duration_cast<std::chrono::nanoseconds>(msg->timestamp.time_since_epoch()).count();
    l.mjd = utils::Time::get_unix_MJD(l.unix_nano);

    try{
    odb::transaction t (_db->begin ());
    _db->persist(l);
    t.commit();
    }catch(const odb::exception& e){
        utils::Log::log(utils::LogMessage::ERROR,class_name,e.what());
    }
}

}
