#define RAPIDJSON_HAS_STDSTRING 1
#include "../../include/json/dispatcher.hpp"
#include "../../include/json/utils.hpp"
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <boost/bind.hpp>

#include "../../include/redist/rapidjson/rapidjson.h"
#include "../../include/redist/rapidjson/writer.h"
#include "../../include/redist/rapidjson/reader.h"
#include "../../include/strip/utils.hpp"
#include "../../include/strip/timer.hpp"
#include "../../include/strip/slo_control.hpp"
#include "../../include/strip/singleton.hpp"

namespace js = rapidjson;
using namespace boost::asio::ip;

#define J_OPCODE "opcode"
#define J_OC_SYSTEM "SYSTEM"
#define J_OC_SLO "SLO"
#define J_OC_TAG "TAG"
#define J_OC_LOG "LOG"
#define J_OC_CRYO "CRYO"

#define J_BOARD_NAME "board"
#define J_POL_NO "pol"
#define J_METHOD "method"
#define J_TYPE "type"
#define J_BASE_ADDR "base_addr"
#define J_DATA "data"
#define J_SIZE "size"
#define J_STATUS "status"
#define J_USER "user"
#define J_TIMEOUT "timeout"

#define J_TAG "tag"
#define J_COMMENT "comment"

#define J_COMMAND "command"
#define J_CMD_ROUND_ALL "round_all_files"
#define J_CMD_ROUND_HDF5 "round_hdf5_files"
#define J_CMD_ROUND_RAW "round_raw_files"

#define J_CRYO_DEVICE "device"

#define J_LEVEL "level"

namespace strip
{

const std::string JsonControl::class_name = "JsonControl";
std::unique_ptr<rapidjson::SchemaDocument> JsonControl::schema;

JsonControl::JsonControl()
{

}

std::string JsonControl::send_status(TagControl::Status status)
{
    js::Document r;
    r.SetObject();
    auto &allocator = r.GetAllocator();
    js::Value res(js::kStringType);

    switch (status)
    {
    case TagControl::Status::ALREADY_STARTED:
        res.SetString("ALREADY_STARTED");
        break;
    case TagControl::Status::UNKNOWN:
        res.SetString("UNKNOWN");
        break;
    case TagControl::Status::OK:
        res.SetString("OK");
        break;
    case TagControl::Status::SINTAX_ERROR:
        res.SetString("SINTAX_ERROR");
        break;
    default:
        res.SetString("BAD STATUS");
        break;
    }
    r.AddMember(J_STATUS, res, allocator);

    js::StringBuffer buffer;
    js::Writer<js::StringBuffer> writer(buffer);
    r.Accept(writer);

    std::string str_msg = buffer.GetString();
    std::cout << "<- " << str_msg << std::endl; //DBG
    return str_msg;
}

std::string JsonControl::send_status(SloCommand::Result status)
{
    js::Document r;
    r.SetObject();
    auto &allocator = r.GetAllocator();

    js::Value res(js::kStringType);
    switch (status)
    {
    case SloCommand::Result::OK:
        res.SetString("OK");
        break;
    case SloCommand::Result::ERROR:
        res.SetString("ERROR");
        break;
    case SloCommand::Result::UNKNOWN_COMMAND:
        res.SetString("UNKNOWN_COMMAND");
        break;
    case SloCommand::Result::ERROR_RO:
        res.SetString("ERROR_RO");
        break;
    case SloCommand::Result::BAD_ADDRESS:
        res.SetString("BAD_ADDRESS");
        break;
    case SloCommand::Result::BAD_TYPE:
        res.SetString("BAD_TYPE");
        break;
    case SloCommand::Result::ERROR_SYNTAX:
        res.SetString("ERROR_SYNTAX");
        break;
    case SloCommand::Result::ERROR_COMM:
        res.SetString("ERROR_COMM");
        break;
    default:
        res.SetString("BAD_STATUS");
        break;
    }

    r.AddMember(J_STATUS, res, allocator);

    js::StringBuffer buffer;
    js::Writer<js::StringBuffer> writer(buffer);
    r.Accept(writer);

    std::string str_msg = buffer.GetString();
    std::cout << "<- " << str_msg << std::endl; //DBG
    return str_msg;
}

std::string JsonControl::handle(const std::string& request)
{

    //decode messages
    std::unique_ptr<js::Document> d(new js::Document());
    d->Parse<0>(request);

    if (d->HasParseError())
        return send_status(SloCommand::Result::ERROR_SYNTAX);

    js::SchemaValidator validator(*schema.get());
    if (!d->Accept(validator))
        return send_status(SloCommand::Result::ERROR_SYNTAX);

    std::string opcode = d->operator[](J_OPCODE).GetString();
    if (opcode == J_OC_SLO)
        return handle_slo(std::move(d));
    else if (opcode == J_OC_CRYO)
        return handle_cryo(std::move(d));
    else if (opcode == J_OC_TAG)
        return handle_tag(std::move(d));
    else if (opcode == J_OC_SYSTEM)
        return handle_cmd(std::move(d));
    else if (opcode == J_OC_LOG)
        return handle_log(std::move(d));
    else
        return send_status(SloCommand::Result::ERROR_SYNTAX);
}

std::string JsonControl::handle_cmd(std::unique_ptr<rapidjson::Document> doc)
{
    js::Document &d = *doc;

    js::Document r;
    r.SetObject();
    auto &allocator = r.GetAllocator();
    js::Value res(js::kStringType);

    if (!d.HasMember(J_COMMAND) || !d[J_COMMAND].IsString())
    {
        res.SetString("SINTAX_ERROR");
    }
    else
    {
        std::string cmd = d[J_COMMAND].GetString();
        if (cmd == J_CMD_ROUND_ALL)
        {
            singletons.timer_hdf5_change_file->force_timeout();
            singletons.timer_raw_change_file->force_timeout();
            res.SetString("OK");
        }
        else if (cmd == J_CMD_ROUND_HDF5)
        {
            singletons.timer_hdf5_change_file->force_timeout();
            res.SetString("OK");
        }
        else if (cmd == J_CMD_ROUND_RAW)
        {
            singletons.timer_raw_change_file->force_timeout();
            res.SetString("OK");
        }
        else
            res.SetString("UNKNOWN_COMMAND");
    }

    r.AddMember(J_STATUS, res, allocator);

    js::StringBuffer buffer;
    js::Writer<js::StringBuffer> writer(buffer);
    r.Accept(writer);

    std::string str_msg = buffer.GetString();
    return str_msg;
}

std::string JsonControl::handle_tag(std::unique_ptr<js::Document> doc)
{
    js::Document &d = *doc;

    if (!d.HasMember(J_TAG) || !d[J_TAG].IsString() || !d.HasMember(J_TYPE) || !d[J_TYPE].IsString() || (d.HasMember(J_COMMENT) && !d[J_COMMENT].IsString()))
    {
        return send_status(TagControl::Status::SINTAX_ERROR);
    }
    Tag t;
    t.tag = d[J_TAG].GetString();
    if (d.HasMember(J_COMMENT))
        t.comment = d[J_COMMENT].GetString();

    std::string tp = d[J_TYPE].GetString();
    if (tp == "START")
    {
        t.type = Tag::Type::START;
    }
    else if (tp == "STOP")
    {
        t.type = Tag::Type::STOP;
    }
    else
    {
        return send_status(TagControl::Status::SINTAX_ERROR);
    }

    return send_status(singletons.tag_control->tag(t));
}

std::string JsonControl::handle_slo(std::unique_ptr<js::Document> doc)
{
    js::Document &d = *doc;

    SloCommand req;
    std::string bname = d[J_BOARD_NAME].GetString();
    if (singletons.config->daq_name.find(bname) == singletons.config->daq_name.end())
        return send_status(SloCommand::Result::ERROR_SYNTAX);
    req.board_name = bname;

    std::string pol_str = d[J_POL_NO].GetString();
    if (pol_str == "BOARD")
        req.pol_no = SloControl::BOARD_ADDR;
    else
    {
        size_t idx = 0;
        for (const auto &p : singletons.config->daq_name.at(bname)->pols)
        {
            if (p == pol_str)
                break;
            else
                idx++;
        }
        if (idx >= singletons.config->daq_name.at(bname)->pols.size())
            return send_status(SloCommand::Result::ERROR_SYNTAX);
        else
            req.pol_no = idx;
    }

    std::string t = d[J_TYPE].GetString();
    if (t == "BIAS")
        req.type = SloCommand::Type::BIAS;
    else if (t == "DAQ")
        req.type = SloCommand::Type::DAQ;
    else if (t == "CRYO")
        req.type = SloCommand::Type::CRYO;
    else
    {
        return send_status(SloCommand::Result::ERROR_SYNTAX);
    }

    t = d[J_METHOD].GetString();
    if (t == "GET")
    {
        if (!d.HasMember(J_SIZE) || !d[J_SIZE].IsInt())
            return send_status(SloCommand::Result::ERROR_SYNTAX);
        req.method = SloCommand::Method::GET;
        int size = d[J_SIZE].GetInt();
        //TODO check for max read registers
        req.data.assign(size, 0);
    }
    else if (t == "SET")
    {
        req.method = SloCommand::Method::SET;
        for (auto i = 0; i < d[J_DATA].Size(); i++)
            req.data.push_back(d[J_DATA][i].GetInt());
    }
    else
    {
        return send_status(SloCommand::Result::BAD_TYPE);
    }

    req.base_addr = d[J_BASE_ADDR].GetString();

    req.user = d[J_USER].GetString();

    if (singletons.slo_boards.find(req.board_name) == singletons.slo_boards.end())
    {
        return send_status(SloCommand::Result::BAD_ADDRESS);
    }

    int timeout = 200;

    if (d.HasMember(J_TIMEOUT))
    {
        if (d[J_TIMEOUT].IsInt())
        {
            timeout = d[J_TIMEOUT].GetInt();
        }
        else
        {
            //TODO report non fatal syntax error
        }
    }

    singletons.slo_boards.at(req.board_name)->send_command(req, timeout);

    js::Document r;
    r.SetObject();
    auto &allocator = r.GetAllocator();

    js::Value board_name(js::kStringType);
    js::Value pol_no(js::kStringType);
    js::Value method(js::kStringType);
    js::Value type(js::kStringType);
    js::Value base_addr(js::kStringType);
    js::Value data(js::kArrayType);
    js::Value user(js::kStringType);
    js::Value res(js::kStringType);

    board_name.SetString(req.board_name, allocator);
    if (req.pol_no == SloControl::BOARD_ADDR)
        pol_no.SetString("BOARD");
    else
    {
        std::string &pn = singletons.config->daq_name.at(req.board_name)->pols[req.pol_no];
        pol_no.SetString(js::StringRef(pn.c_str(), pn.size()));
    }
    switch (req.method)
    {
    case SloCommand::Method::GET:
        method.SetString("GET");
        break;
    case SloCommand::Method::SET:
        method.SetString("SET");
        break;
    default:
        break;
    }

    switch (req.type)
    {
    case SloCommand::Type::BIAS:
        type.SetString("BIAS");
        break;
    case SloCommand::Type::DAQ:
        type.SetString("DAQ");
        break;
    case SloCommand::Type::CRYO:
        type.SetString("CRYO");
        break;
    default:
        break;
    }

    base_addr.SetString(req.base_addr, allocator);
    user.SetString(req.user, allocator);
    switch (req.res)
    {
    case SloCommand::Result::OK:
        res.SetString("OK");
        break;
    case SloCommand::Result::ERROR:
        res.SetString("ERROR");
        break;
    case SloCommand::Result::UNKNOWN_COMMAND:
        res.SetString("UNKNOWN_COMMAND");
        break;
    case SloCommand::Result::ERROR_RO:
        res.SetString("ERROR_RO");
        break;
    case SloCommand::Result::BAD_ADDRESS:
        res.SetString("BAD_ADDRESS");
        break;
    case SloCommand::Result::BAD_TYPE:
        res.SetString("BAD_TYPE");
        break;
    case SloCommand::Result::ERROR_SYNTAX:
        res.SetString("ERROR_SYNTAX");
        break;
    case SloCommand::Result::ERROR_COMM:
        res.SetString("ERROR_COMM");
        break;
    case SloCommand::Result::ERROR_TIMEOUT_GET:
        res.SetString("ERROR_TIMEOUT_GET");
        break;
    case SloCommand::Result::ERROR_TIMEOUT_SET:
        res.SetString("ERROR_TIMEOUT_SET");
        break;
    case SloCommand::Result::ERROR_DATA:
        res.SetString("ERROR_DATA");
        break;
    default:
        break;
    }

    //FIXME find a smart way
    std::map<uint8_t, std::list<addr_item>::const_iterator> *u_map = nullptr;
    std::map<std::string, std::list<addr_item>::const_iterator> *s_map = nullptr;
    if (req.type == SloCommand::Type::BIAS)
    {
        if (req.pol_no < 8)
        {
            u_map = &singletons.config->map_bias_pol_u8;
            s_map = &singletons.config->map_bias_pol_str;
        }
        else
        {
            u_map = &singletons.config->map_bias_sys_u8;
            s_map = &singletons.config->map_bias_sys_str;
        }
    }
    if (req.type == SloCommand::Type::DAQ)
    {
        if (req.pol_no < 8)
        {
            u_map = &singletons.config->map_daq_pol_u8;
            s_map = &singletons.config->map_daq_pol_str;
        }
        else
        {
            u_map = &singletons.config->map_daq_sys_u8;
            s_map = &singletons.config->map_daq_sys_str;
        }
    }
    uint8_t i = 0;
    if (s_map && s_map->find(req.base_addr) != s_map->end())
        i = s_map->at(req.base_addr)->address;

    for (auto v : req.data)
    {
        if (u_map && u_map->find(i) != u_map->end())
        {
            if (u_map->at(i)->type == addr_item::i16)
            {
                int16_t iv = v;
                data.PushBack(iv, allocator);
            }
            else
            {
                data.PushBack(v, allocator);
            }
        }
        else
        {
            data.PushBack(v, allocator);
        }
        i++;
    }

    r.AddMember(J_BOARD_NAME, board_name, allocator);
    r.AddMember(J_POL_NO, pol_no, allocator);
    r.AddMember(J_METHOD, method, allocator);
    r.AddMember(J_TYPE, type, allocator);
    r.AddMember(J_BASE_ADDR, base_addr, allocator);
    r.AddMember(J_DATA, data, allocator);
    r.AddMember(J_USER, user, allocator);
    r.AddMember(J_STATUS, res, allocator);

    js::StringBuffer buffer;
    js::Writer<js::StringBuffer> writer(buffer);
    r.Accept(writer);

    std::string str_msg = buffer.GetString();
    std::cout << "<- " << str_msg << std::endl; //DBG
    return str_msg;
}

std::string JsonControl::handle_cryo(std::unique_ptr<js::Document> doc)
{
    js::Document &d = *doc;

    auto cmd = load(d);
    std::string str_msg;
    std::string device = d[J_CRYO_DEVICE].GetString();

    if (singletons.cryo_units.find(device) == singletons.cryo_units.end())
    { // device not found, return error
        cmd.as_map()["status"] = "ERROR_NO_DEVICE";
        cmd.as_map()["error"] = "no device '"+device+"' was found";
        str_msg = serialize(cmd);
    }
    else
    {
        auto &unit = singletons.cryo_units.at(device);
        auto res = unit->send_command(cmd);
        str_msg = serialize(res);
    }
    std::cout << "<- " << str_msg << std::endl; //DBG
    return str_msg;
}

std::string JsonControl::handle_log(std::unique_ptr<js::Document> doc)
{
    js::Document &d = *doc;

    utils::LogMessage::Level l;

    std::string lev = d[J_LEVEL].GetString();
    std::string user = d["user"].GetString();
    std::string message = d["message"].GetString();

    if (lev == "INFO")
        l = utils::LogMessage::Level::INFO;
    else if (lev == "DEBUG")
        l = utils::LogMessage::Level::DEBUG;
    else if (lev == "WARNING")
        l = utils::LogMessage::Level::WARNING;
    else
        l = utils::LogMessage::Level::ERROR;

    utils::Log::log(l, "user", message, user);

    js::Document r;
    r.SetObject();

    js::Value res(js::kStringType);
    res.SetString("OK");

    r.AddMember(J_STATUS, res, r.GetAllocator());

    js::StringBuffer buffer;
    js::Writer<js::StringBuffer> writer(buffer);
    r.Accept(writer);

    std::string str_msg = buffer.GetString();
    std::cout << "<- " << str_msg << std::endl; //DBG
    return str_msg;
}

int JsonControl::init()
{

    std::ifstream ifs(singletons.config->json_schema_path_slo);
    std::string schema_str((std::istreambuf_iterator<char>(ifs)),
                           (std::istreambuf_iterator<char>()));
    ifs.close();

    js::Document sd;
    if (sd.Parse(schema_str.c_str()).HasParseError())
    {
        utils::Log::log(utils::LogMessage::Level::ERROR, class_name, "invalid json_schema_slo");
        return 1;
    }
    else
    { // add addresses
        auto &all = sd.GetAllocator();
        //append board names to definitions.slo.defs.allOf[0].properties
        std::pair<std::list<strip::addr_item> &, const char *> l[] = {
            {singletons.config->address_bias_sys, "bias_board"},
            {singletons.config->address_bias_pol, "bias_pol"},
            {singletons.config->address_daq_pol, "daq_pol"},
            {singletons.config->address_daq_sys, "daq_board"},
        };

        for (auto &i : l)
        {
            js::Value addr_array(js::kArrayType);
            for (const auto &addr : i.first)
            {
                js::Value a(js::kStringType);
                a.SetString(js::StringRef(addr.name.c_str(), addr.name.size()));
                addr_array.PushBack(a, all);
            }
            js::Value base_addr(js::kObjectType);
            base_addr.AddMember("enum", addr_array, all);
            sd["definitions"]["slo"]["defs"][i.second]["properties"].AddMember("base_addr", base_addr, all);
        }

        //get board and polarimeter names from configuration
        js::Value board_array(js::kArrayType);
        for (auto &board : singletons.config->daq_name)
        {
            std::string &s = board.second->name;
            js::Value a(js::kStringType);
            a.SetString(js::StringRef(s.c_str(), s.size()));
            board_array.PushBack(a, all);
        }
        js::Value board_obj(js::kObjectType);
        board_obj.AddMember("enum", board_array, all);
        sd["definitions"]["slo"]["allOf"][0]["properties"].AddMember("board", board_obj, all);

        js::Value pol_array_bias(js::kArrayType);
        js::Value pol_array_allof(js::kArrayType);
        for (auto &board : singletons.config->daq_name)
        {
            for (const std::string &pol : board.second->pols)
            {
                js::Value a(js::kStringType);
                js::Value b(js::kStringType);
                a.SetString(js::StringRef(pol.c_str(), pol.size()));
                b.SetString(js::StringRef(pol.c_str(), pol.size()));
                pol_array_bias.PushBack(a, all);
                pol_array_allof.PushBack(b, all);
            }
        }
        pol_array_allof.PushBack("BOARD", all);

        js::Value pol_bias_obj(js::kObjectType);
        pol_bias_obj.AddMember("enum", pol_array_bias, all);
        sd["definitions"]["slo"]["defs"]["bias_pol"]["properties"].AddMember("pol", pol_bias_obj, all);

        js::Value pol_allof_obj(js::kObjectType);
        pol_allof_obj.AddMember("enum", pol_array_allof, all);
        sd["definitions"]["slo"]["allOf"][0]["properties"].AddMember("pol", pol_allof_obj, all);

    }
    JsonControl::schema.reset(new js::SchemaDocument(sd));
    return 0;
}

} // namespace strip
