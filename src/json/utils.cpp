#include "../../include/json/utils.hpp"
#include "../../include/redist/rapidjson/writer.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <string>
#include <list>

namespace js = rapidjson;

class json_visitor : public boost::static_visitor<void>
{
public:
    json_visitor() = default;

    void operator()(const bool &i)
    {
        _s += i ? "true" : "false";
    }

    void operator()(const int64_t &i)
    {
        _s += std::to_string(i);
    }

    void operator()(const uint64_t &i)
    {
        _s += std::to_string(i);
    }

    void operator()(const double &i)
    {
        _s += std::to_string(i);
    }

    void operator()(const std::string &i)
    {
        std::stringstream ss;
        ss << std::quoted(i);
        _s += ss.str();
    }
    void operator()(const boost::basic_string_ref<char, std::char_traits<char>> &i)
    {
        std::cout << "NOT IMPLEMENTED:"
                  << "operator()(const boost::basic_string_ref<char, std::char_traits<char> >&i)" << std::endl;
    }

    void operator()(const std::vector<char> &)
    {
        std::cout << "NOT IMPLEMENTED:"
                  << "operator()(const std::vector<char>& )" << std::endl;
    }

    void operator()(const msgpack::v1::type::raw_ref &)
    {
        std::cout << "NOT IMPLEMENTED:"
                  << "operator()(const msgpack::v1::type::raw_ref& )" << std::endl;
    }

    void operator()(const msgpack::v1::type::ext &)
    {
        std::cout << "NOT IMPLEMENTED:"
                  << "operator()(const msgpack::v1::type::ext& )" << std::endl;
    }

    void operator()(const std::vector<msgpack::type::variant> &i)
    {
        _s += "[";
        if (!i.empty())
        {
            for (const auto &p : i)
            {
                boost::apply_visitor(*this, p);
                _s += ",";
            }
            _s.erase(_s.size() - 1, 1);
        }
        _s += "]";
    }

    void operator()(const std::map<msgpack::type::variant, msgpack::type::variant> &i)
    {
        _s += "{";
        if (!i.empty())
        {
            for (const auto &p : i)
            {
                boost::apply_visitor(*this, p.first);
                _s += ":";
                boost::apply_visitor(*this, p.second);
                _s += ",";
            }
            _s.erase(_s.size() - 1, 1);
        }
        _s += "}";
    }

    void operator()(const std::multimap<msgpack::type::variant, msgpack::type::variant> &i)
    {
        _s += "{";
        if (!i.empty())
        {
            for (const auto &p : i)
            {
                boost::apply_visitor(*this, p.first);
                _s += ":";
                boost::apply_visitor(*this, p.second);
                _s += ",";
            }
            _s.erase(_s.size() - 1, 1);
        }
        _s += "}";
    }

    void operator()(const msgpack::v1::type::nil_t &)
    {
        std::cout << "NOT IMPLEMENTED:"
                  << "operator()(const msgpack::v1::type::nil_t& )" << std::endl;
    }

    std::string str() { return _s; }

private:
    std::string _s;
};

class VarWriter
{
    typedef enum{VOID,ARRAY,MAP} type;
public:
    typedef msgpack::type::variant var_t;

    VarWriter(){
        _curr.push_back({VOID,&_obj});
    }

    bool Null(){return true;}
    bool Bool(bool b){ return push(b); }
    bool Int(int i){ return push(i); }
    bool Uint(unsigned i){ return push(i); }
    bool Int64(int64_t i){ return push(i); }
    bool Uint64(uint64_t i){ return push(i); }
    bool Double(double d){ return push(d); }
    /// enabled via kParseNumbersAsStringsFlag, string is not null-terminated (use length)
    bool RawNumber(const char *str, js::SizeType length, bool copy){return false;}
    bool String(const char *str, js::SizeType length, bool copy){ return push(std::string(str)); }
    bool StartObject(){
        auto c = _curr.back();
        if(c.first == type::VOID){ // root item
            _obj = std::map<var_t,var_t>();
            _curr.back().first = type::MAP;
        }else if(c.first == type::MAP){
            c.second->as_map()[_key]=std::map<var_t,var_t>();
            _curr.push_back({type::MAP,&c.second->as_map()[_key]});
        }else if(c.first == type::ARRAY){
            c.second->as_vector().push_back(std::map<var_t,var_t>());
            _curr.push_back({type::MAP,&c.second->as_vector().back()});
        }else
            return false;
        return true;
    }
    bool Key(const char *str, js::SizeType length, bool copy){_key = str; return true;}
    bool EndObject(js::SizeType memberCount){ _curr.pop_back();return true;}
    bool StartArray(){
        auto c = _curr.back();
        if(c.first == type::VOID){ // root item
            _obj = std::vector<var_t>();
            _curr.back().first = type::ARRAY;
        }else if(c.first == type::MAP){
            c.second->as_map()[_key]=std::vector<var_t>();
            _curr.push_back({type::ARRAY,&c.second->as_map()[_key]});
        }else if(c.first == type::ARRAY){
            c.second->as_vector().push_back(std::vector<var_t>());
            _curr.push_back({type::ARRAY,&c.second->as_vector().back()});
        }else
            return false;
        return true;       
    }
    bool EndArray(js::SizeType elementCount){ _curr.pop_back();return true;}

    var_t obj(){return _obj;}
private:
    template<typename T>
    bool push(T item){
        auto c = _curr.back();
        if(c.first == type::MAP)
            c.second->as_map()[_key]=item;
        else if(c.first == type::ARRAY)
            c.second->as_vector().push_back(item);
        else
            return false;
        return true;
    }

    std::list<std::pair<type,var_t*>> _curr;
    var_t _obj;
    std::string _key;
};

std::string serialize(const msgpack::type::variant &pack)
{
    json_visitor vis;
    boost::apply_visitor(vis, pack);
    return vis.str();
}

msgpack::type::variant load(const rapidjson::Document &doc)
{
    VarWriter wr;
    doc.Accept(wr);
    
    return wr.obj();
}