#include "../../include/autobahn/base.hpp"
#include "../../include/strip/utils.hpp"

namespace strip{
namespace autobahn{

Base::Base(const boost::asio::ip::address& addr,uint16_t port) :
    _raw_endpoint(addr,port){
    _wamp = std::make_shared<::autobahn::wamp_tcp_transport>(strip::utils::ioService::service, _raw_endpoint);
}

boost::future<int> Base::init(const std::string& realm){
    _session = std::make_shared<::autobahn::wamp_session>(strip::utils::ioService::service);
    _wamp->attach(std::static_pointer_cast<::autobahn::wamp_transport_handler>(_session));
    boost::future<int> connect_future = _wamp->connect().then([realm,this](boost::future<void> connected) -> int{
            std::unique_lock<std::mutex> l(_m); // lock the connection
            try{
                connected.get(); // wait for connection and throw on error

                boost::future<void> start_future = _session->start();
                start_future.get();// wait for session and throw on error

                boost::future<uint64_t> join_future = _session->join(realm);
                std::cout << "joined realm:" << join_future.get() << std::endl; // wait for join realm and throw on error
                return 0;

            } catch (const std::exception& e) {
                std::cerr << e.what() << std::endl;
                return 1;
            }
    });

    return connect_future;
}


void Base::leave(){
    boost::future<std::string> reason = _session->leave();
    try{
        std::cerr << "left session (" << reason.get() << ")" << std::endl;
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
}

}
}
