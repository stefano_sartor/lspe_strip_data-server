#include "../../include/autobahn/tag.hpp"

typedef struct{
    int64_t id;
    std::string tag;
    std::string start_comment;
    std::string stop_comment;
    double start;
    double stop;
} TagEntry;


namespace strip{
namespace autobahn{

const std::string Tag::class_name = "autobahn::Tag";


Tag::Tag(const boost::asio::ip::address& addr,uint16_t port) : Base(addr,port) {
    _conn_close = utils::UnixSignals::halt3_ext().connect(std::bind(&Tag::slot_close, this));
}


void Tag::connect_tag(signal_tag &sig){
    _conn_tag = sig.connect(std::bind(&Tag::slot_tag, std::ref(*this),std::placeholders::_1));
}

int Tag::slot_close(){
    _conn_close.reset();
    leave();
    return  0;
}

void Tag::slot_tag(std::shared_ptr<const DbTag> t){
    TagEntry e;

    e.id            = t->db_id;
    e.tag           = t->tag;
    e.start_comment = t->comment_start;
    e.stop_comment  = t->comment_stop;
    e.start         = t->mjd_start;
    e.stop          = t->mjd_stop;

    std::tuple<int> arguments;
    publish("strip.tag", arguments,e);
}


} //autobahn
} //strip


#include <msgpack.hpp>

namespace msgpack {
MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
namespace adaptor {

template <>
struct object_with_zone<TagEntry> {
    void operator()(msgpack::object::with_zone& o, TagEntry const& v) const {
        o.type = msgpack::type::MAP;
        uint32_t size = msgpack::checked_get_container_size(6);
        msgpack::object_kv* p = static_cast<msgpack::object_kv*>(o.zone.allocate_align(sizeof(msgpack::object_kv)*size, MSGPACK_ZONE_ALIGNOF(msgpack::object_kv)));
        o.via.map.ptr  = p;
        o.via.map.size = size;

        p[0].key = msgpack::object("id",o.zone);
        p[0].val = msgpack::object(v.id,o.zone);

        p[1].key = msgpack::object("tag",o.zone);
        p[1].val = msgpack::object(v.tag,o.zone);

        p[2].key = msgpack::object("start_comment",o.zone);
        p[2].val = msgpack::object(v.start_comment,o.zone);

        p[3].key = msgpack::object("stop_comment",o.zone);
        p[3].val = msgpack::object(v.stop_comment,o.zone);

        p[4].key = msgpack::object("start",o.zone);
        p[4].val = msgpack::object(v.start,o.zone);

        p[5].key = msgpack::object("stop",o.zone);
        p[5].val = msgpack::object(v.stop,o.zone);
    }
};

} // namespace adaptor
} // MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS)
} // namespace msgpack
