#include "../../include/autobahn/daq_pkt.hpp"
#include "../../include/strip/singleton.hpp"
#include "../../include/strip/config.hpp"
#include "../../include/strip/utils.hpp"

#define MSGPACK_USE_BOOST 1
#include <boost/variant.hpp>
#include <msgpack.hpp>

namespace strip{
namespace autobahn{

const std::string DaqPkt::class_name = "autobahn::DaqPkt";

DaqPkt::DaqPkt(const boost::asio::ip::address& addr,uint16_t port) :
    Base(addr,port)
{
    _conn_close = utils::UnixSignals::halt3_ext().connect(std::bind(&DaqPkt::slot_close, this));
}


void DaqPkt::connect_pkt(signal_pkt& sig){
    signal_pkt::Connection conn = sig.connect(std::bind(&DaqPkt::slot_pkt, std::ref(*this),std::placeholders::_1));
    _conn_pkt.push_back(conn);
}

int DaqPkt::slot_close(){
    _conn_close.reset();
    leave();
}

void DaqPkt::slot_pkt(std::shared_ptr<const DaqUdpPkt> pkt){
    typedef std::map<msgpack::type::variant,msgpack::type::variant> mpmap;
    std::string base_topic = "strip.fp";
    for(const strip::DaqPkt& p: pkt->pkts){

        double m_jd = utils::Time::get_MJD(p.timestamp);

        for(size_t i=0; i<singletons.config->daq_id.at(p.board_id)->pols.size(); i++){
            msgpack::type::variant pkt = mpmap();
            mpmap& up = pkt.as_map();


            up["pol"] = singletons.config->daq_id.at(p.board_id)->pols[i];
            up["mjd"] = m_jd;

            up["DEMQ1"] = p.dem_Q1(i);
            up["DEMU1"] = p.dem_U1(i);
            up["DEMU2"] = p.dem_U2(i);
            up["DEMQ2"] = p.dem_Q2(i);

            up["PWRQ1"] = p.pwr_Q1(i);
            up["PWRU1"] = p.pwr_U1(i);
            up["PWRU2"] = p.pwr_U2(i);
            up["PWRQ2"] = p.pwr_Q2(i);

            up["PHB"]      = p.phb;
            up["BRST_PLS"] = p.brst_pls;
            up["BRST_LAT"] = p.brst_lat;


            std::tuple<int> arguments;
            std::string topic = base_topic + ".pol." + boost::get<std::string>(up["pol"]);
            publish(topic, arguments,pkt);
        }
    }
}



}
}
