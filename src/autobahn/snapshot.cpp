#include "../../include/autobahn/snapshot.hpp"
#include "../../include/strip/singleton.hpp"
//#include "autobahn_upkt.hpp"
#include <boost/variant/get.hpp>
#include <stdexcept>
#include <deque>

namespace strip {
namespace autobahn{

const std::string Snapshot::class_name = "autobahn::Snapshot";
const std::string Snapshot::sh_mem_name = "StripSnapshot";
const std::string Snapshot::sh_vec_name = "snapshot";

Snapshot::~Snapshot(){
    boost::interprocess::shared_memory_object::remove(sh_mem_name.c_str());
}

Snapshot::Snapshot(){
    namespace ipc = boost::interprocess;

    ipc::shared_memory_object::remove(sh_mem_name.c_str());

    _conn_close = utils::UnixSignals::halt3_ext().connect(std::bind(&Snapshot::slot_close, this));
    const Config& conf = *singletons.config;

    std::size_t el_count = 0;
    for(const auto& board : conf.daq_name){
        for(const std::string& pol : board.second->pols){
            el_count += conf.address_bias_pol.size();
            el_count += conf.address_daq_pol.size();
            el_count += 8; //4 DEM and 4 PWR
        }
        el_count += conf.address_bias_sys.size();
        el_count += conf.address_daq_sys.size();
    }
    std::cout << "SNAPSHOT SIZE: " << el_count << " * sizeof " << sizeof(value_pod) << " = " << el_count*sizeof(value_pod) << std::endl; //DBG
    _segment = ipc::managed_shared_memory(ipc::create_only,sh_mem_name.c_str(),4096+el_count*sizeof(value_pod));
    sh_allocator alloc_inst (_segment.get_segment_manager());
    _sh_vec.reset(_segment.construct<sh_vector>(sh_vec_name.c_str())(el_count,value_pod(),alloc_inst));

    auto set_pod = [this](const std::string& board,const std::string& pol,const std::string& table,const std::string& addr, const auto& v, size_t idx){
        value_pod& pod = _sh_vec->at(idx);
        board.copy(pod.board,pod.BUFF);
        pol.copy(pod.pol,pod.BUFF);
        table.copy(pod.table,pod.BUFF);
        addr.copy(pod.addr,pod.BUFF);
        pod.val = v;
    };
    size_t idx = 0;
    for(const auto& board : conf.daq_name){
        for(const std::string& pol : board.second->pols){
            for(const auto& item : conf.address_bias_pol){
                _snapshot[board.second->name][pol]["BIAS"][item.name.c_str()] = &_sh_vec->at(idx);
                set_pod(board.second->name,pol,"BIAS",item.name,uint16_t(0),idx++);
            }
            for(const auto& item : conf.address_daq_pol){
                _snapshot[board.second->name][pol]["DAQ"][item.name.c_str()] = &_sh_vec->at(idx);
                set_pod(board.second->name,pol,"DAQ",item.name,uint16_t(0),idx++);
            }
            _snapshot[board.second->name][pol]["SCI"]["DEMQ1"] = &_sh_vec->at(idx);
            set_pod(board.second->name,pol,"SCI","DEMQ1",float(0),idx++);

            _snapshot[board.second->name][pol]["SCI"]["DEMU1"] = &_sh_vec->at(idx);
            set_pod(board.second->name,pol,"SCI","DEMU1",float(0),idx++);

            _snapshot[board.second->name][pol]["SCI"]["DEMU2"] = &_sh_vec->at(idx);
            set_pod(board.second->name,pol,"SCI","DEMU2",float(0),idx++);

            _snapshot[board.second->name][pol]["SCI"]["DEMQ2"] = &_sh_vec->at(idx);
            set_pod(board.second->name,pol,"SCI","DEMQ2",float(0),idx++);


            _snapshot[board.second->name][pol]["SCI"]["PWRQ1"] = &_sh_vec->at(idx);
            set_pod(board.second->name,pol,"SCI","PWRQ1",float(0),idx++);

            _snapshot[board.second->name][pol]["SCI"]["PWRU1"] = &_sh_vec->at(idx);
            set_pod(board.second->name,pol,"SCI","PWRU1",float(0),idx++);

            _snapshot[board.second->name][pol]["SCI"]["PWRU2"] = &_sh_vec->at(idx);
            set_pod(board.second->name,pol,"SCI","PWRU2",float(0),idx++);

            _snapshot[board.second->name][pol]["SCI"]["PWRQ2"] = &_sh_vec->at(idx);
            set_pod(board.second->name,pol,"SCI","PWRQ2",float(0),idx++);

        }
        for(const auto& item : conf.address_bias_sys){
            _snapshot[board.second->name]["SYS"]["BIAS"][item.name.c_str()] = &_sh_vec->at(idx);
            set_pod(board.second->name,"SYS","BIAS",item.name,uint16_t(0),idx++);
        }
        for(const auto& item : conf.address_daq_sys){
            _snapshot[board.second->name]["SYS"]["DAQ"][item.name.c_str()] = &_sh_vec->at(idx);
            set_pod(board.second->name,"SYS","DAQ",item.name,uint16_t(0),idx++);
        }
    }

    _alpha = 2.0f/(100.0f+1.0f); //FIXME read from config 2/(N+1)
}


void Snapshot::connect_pkt(signal_pkt& sig){
    signal_pkt::Connection conn = sig.connect(std::bind(&Snapshot::slot_pkt, std::ref(*this),std::placeholders::_1));
    _conn_pkt.push_back(conn);
}

int Snapshot::slot_close(){
    _conn_close.reset();
}

void Snapshot::update(const addr_map &src, addr_map_ptr &dst){
    for(const auto& item : src)
        dst[item.first]->val=item.second;
}

template<class A,typename T>
inline void wma(A& acc,const T& val,const float& alpha){
    acc = static_cast<float>((alpha * val) + (1.0 - alpha) * boost::get<float>(acc));
}

void Snapshot::slot_pkt(std::shared_ptr<const DaqUdpPkt> pkt){
    std::string addr_name;

    for(const strip::DaqPkt& p: pkt->pkts){
       // std::string table_name;
        const std::string& board_name = singletons.config->daq_id.at(p.board_id)->name;
       /* addr_map hk_map;
        bool hk_not_empty = false;

        std::map<uint8_t,std::list<addr_item>::const_iterator> * addr_map = nullptr;
        if( p.hk.hk_src == strip::DaqPkt::HK_SRC_BIAS){
          if( p.hk.table_id != strip::DaqPkt::HK_TABLE_BOARD )
            addr_map = &singletons.config->map_bias_pol_u8;
          else
            addr_map = &singletons.config->map_bias_sys_u8;
            table_name = "BIAS";
        }else{
          if( p.hk.table_id != strip::DaqPkt::HK_TABLE_BOARD )
            addr_map = &singletons.config->map_daq_pol_u8;
          else
            addr_map = &singletons.config->map_daq_sys_u8;
            table_name = "DAQ";
        }

        for (auto i=0; i<HouseKeeping::HK_SIZE; i++) {
          uint8_t address = p.hk.base_addr+i;
          if(addr_map->find(address) == addr_map->end())
            continue;

          const std::string& name = addr_map->at(address)->name;
          if(addr_map->at(address)->type == addr_item::Type::u16){
              hk_map[name.c_str()] = p.hk.hk[i].u_val;
          }else{
              hk_map[name.c_str()] = p.hk.hk[i].i_val;
          }
        }

        hk_not_empty = ! hk_map.empty(); // if HK registers have meaning, add to HK obj
*/
        for(size_t i=0; i<singletons.config->daq_id.at(p.board_id)->pols.size(); i++){
            const std::string& pol_name = singletons.config->daq_id.at(p.board_id)->pols[i];
/*            if(i ==  p.hk.table_id && hk_not_empty){
                  update(hk_map,_snapshot[board_name][pol_name][table_name]);
            }
            */
            wma(_snapshot[board_name][pol_name]["SCI"]["DEMQ1"]->val,p.dem_Q1(i),_alpha);
            wma(_snapshot[board_name][pol_name]["SCI"]["DEMU1"]->val,p.dem_U1(i),_alpha);
            wma(_snapshot[board_name][pol_name]["SCI"]["DEMU2"]->val,p.dem_U2(i),_alpha);
            wma(_snapshot[board_name][pol_name]["SCI"]["DEMQ2"]->val,p.dem_Q2(i),_alpha);

            wma(_snapshot[board_name][pol_name]["SCI"]["PWRQ1"]->val,p.pwr_Q1(i),_alpha);
            wma(_snapshot[board_name][pol_name]["SCI"]["PWRU1"]->val,p.pwr_U1(i),_alpha);
            wma(_snapshot[board_name][pol_name]["SCI"]["PWRU2"]->val,p.pwr_U2(i),_alpha);
            wma(_snapshot[board_name][pol_name]["SCI"]["PWRQ2"]->val,p.pwr_Q2(i),_alpha);
        }

/*        if(p.hk.table_id == strip::DaqPkt::HK_TABLE_BOARD && hk_not_empty){ // do not send HK empty
            autobahn::uBoardPkt up;
            update(hk_map,_snapshot[board_name]["SYS"][table_name]);
        }*/
    }
}


}
}

