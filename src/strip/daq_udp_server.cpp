#include "../../include/strip/daq_udp_server.hpp"
#include "../../include/strip/singleton.hpp"
#include "../../include/strip/utils.hpp"
#include <boost/bind.hpp>
#include <iostream>
#include <limits>
using boost::asio::ip::udp;

namespace strip {

DaqUdpServer::DaqUdpServer(const boost::asio::ip::address &addr, uint16_t port)
    : _warmup_pkts(200),
      _socket(strip::utils::ioService::service, udp::endpoint(udp::v4(), port)),
      _remote_endpoint(addr, port), _pkt_sig(singletons.workers_pool) {
  timerclear(&_ts_offset);
  _conn_close = utils::UnixSignals::halt3_ext().connect(
      std::bind(&DaqUdpServer::slot_close, this));
  start_receive();
}

void DaqUdpServer::start_receive() {
  _udp_pkt.reset(new DaqUdpPkt());
  _socket.async_receive_from(
      boost::asio::buffer(_udp_pkt->buffer), _remote_endpoint,
      boost::bind(&DaqUdpServer::handle_receive, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));
}

void DaqUdpServer::handle_receive(const boost::system::error_code &error,
                                  std::size_t bytes_transferred) {
  if (!error) {
    if (bytes_transferred != (DAQ_UDP_PKTS * DAQ_PKT_LEN)) {
      std::string err = "packet lenght error. " +
                        std::to_string(bytes_transferred) + " instead of " +
                        std::to_string(DAQ_UDP_PKTS * DAQ_PKT_LEN);
      utils::Log::log(utils::LogMessage::Level::ERROR, "DaqUdpServer", err);
    } else if (_warmup_pkts > 0) {
      _warmup_pkts--;
    } else {
      // decode messages
      bool ok = true;
      //	std::cout << "RECEIVED UDP, BYTES:" << bytes_transferred <<
      // std::endl;
      for (auto i = 0; i < DAQ_UDP_PKTS; i++) {
        bool good = _udp_pkt->pkts[i].decode(
            &_udp_pkt->buffer[DAQ_PKT_LEN * i],
#ifdef USE_FAKE_GPS
            &_udp_pkt->buffer[DAQ_PKT_LEN * (i + 1)], &_ts_offset);
#else
            // nullptr means no offset to apply, i.e. time packet is used
            &_udp_pkt->buffer[DAQ_PKT_LEN * (i + 1)], nullptr);
#endif
        if (!good) {
          utils::Log::log(utils::LogMessage::Level::ERROR, "DaqUdpServer",
                          "decode packet error, skip UDP packet");
          ok = false;
          break;
        }
      }
      if (ok) {
        _pkt_sig(_udp_pkt);
      }
    }
  } else {
    utils::Log::log(utils::LogMessage::Level::ERROR, "DaqUdpServer",
                    "ASIO error " + error.message());
  }
  start_receive();
}

int DaqUdpServer::slot_close() {
  _socket.close();
  _conn_close.reset();
  return 0;
}

} // namespace strip
