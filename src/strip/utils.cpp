#include "../../include/strip/utils.hpp"
#include "../../include/strip/singleton.hpp"
#include "../../include/strip/slo_control.hpp"
#include "../../include/json/dispatcher.hpp"
#include "../../include/cryo/lakeshore_tcp.hpp"
#include "../../include/cryo/lakeshore_serial.hpp"
#include "../../include/cryo/multiplexer.hpp"

#include <ctime>
#include <erfa.h>
#include <sys/time.h>
#include <csignal>
#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <stdexcept>
#include <string>

using boost::property_tree::ptree;

void wait_signal(strip::utils::signal_halt &sig, const std::string &message)
{
    using strip::utils::Log;
    using strip::utils::LogMessage;
    typedef strip::utils::signal_halt::deque_future deque;

    deque q = sig.operator()();
    Log::log(LogMessage::Level::INFO, "CLOSE", "closing " + std::to_string(q.size()) + message);
    for (auto &f : q)
    {
        try
        {
            f.wait();
            int i = f.get(); //TODO do something usefull with the result
        }
        catch (std::future_error &e)
        {
            std::cout << "signal connection deleted while closing: " << e.code().message() << '\n';
        }
    }
}

#include <thread>
void handle_signal(int sig)
{
    switch (sig)
    {
    case SIGINT:
        wait_signal(strip::utils::UnixSignals::halt3_ext(), " external connections");
        //std::this_thread::sleep_for (std::chrono::seconds(1));
        wait_signal(strip::utils::UnixSignals::halt2_ops(), " operations");
        //std::this_thread::sleep_for (std::chrono::seconds(1));
        wait_signal(strip::utils::UnixSignals::halt1_db(), " database connections");
        //std::this_thread::sleep_for (std::chrono::seconds(1));
        wait_signal(strip::utils::UnixSignals::halt0_log(), " log system");
        //std::this_thread::sleep_for (std::chrono::seconds(1));

        std::cout << "...shutting down server..." << std::endl;

        strip::singletons.reset_all();

        break;
    default:
        break;
    }
}

namespace strip
{

    namespace utils
    {

        boost::asio::io_service ioService::service;
        std::unique_ptr<Log> Log::_singleton;

        void parse_config(const std::string &path)
        {
            std::ifstream file(path);
            std::stringstream buffer;
            if (!file)
            {
                throw std::runtime_error("Could not open config file: " + path);
            }

            buffer << file.rdbuf();
            file.close();

            ptree pt;
            read_json(buffer, pt);

            singletons.config.reset(new Config());

            Config &c = *singletons.config.get();

            c.workers = pt.get<size_t>("data-server.workers");
            c.db_name = pt.get<std::string>("db.database");
            c.db_user = pt.get<std::string>("db.user");
            c.db_password = pt.get<std::string>("db.password");
            c.db_ip = pt.get<std::string>("db.ip");
            c.db_port = pt.get<uint16_t>("db.port");

            c.wamp_router_ip = pt.get<std::string>("wamp.ip");
            c.wamp_router_port = pt.get<uint16_t>("wamp.port");
            c.wamp_realm = pt.get<std::string>("wamp.realm");

            size_t i = 0;
            for (auto &e : pt.get_child("daq_boards"))
            {
                std::shared_ptr<DaqConfig> board(new DaqConfig);

                board->id = e.second.get<char>("id");
                board->name = e.second.get<std::string>("name");
                for (auto &pol : e.second.get_child("pols"))
                    board->pols.push_back(pol.second.get_value<std::string>());

                board->sci_ip = e.second.get<std::string>("sci.ip");
                board->sci_port = e.second.get<uint16_t>("sci.port");

                board->slo_ip = e.second.get<std::string>("slo.ip");
                board->slo_port = e.second.get<uint16_t>("slo.port");

                c.daq_id[board->id] = board;
                c.daq_name[board->name] = board;
            }

            for (auto &unit : pt.get_child("cryo.devices"))
            {
                std::string conf_path = unit.second.get<std::string>("config");
                std::ifstream cryo_conf(conf_path);
                std::stringstream buffer;
                if (!cryo_conf)
                {
                    throw std::runtime_error("Could not open config file: " + conf_path);
                }

                buffer << cryo_conf.rdbuf();
                cryo_conf.close();

                ptree pt;
                read_json(buffer, pt);

                c.cryo[unit.second.get<std::string>("name")] = pt;
            }

            c.gps.first = pt.get<std::string>("gps.ip");
            c.gps.second = pt.get<uint16_t>("gps.port");

            c.timer_raw_flush = pt.get<uint64_t>("data-server.raw_pkt.flush_timeout");
            c.timer_raw_change_file = pt.get<uint64_t>("data-server.raw_pkt.change_file");
            c.raw_path = pt.get<std::string>("data-server.raw_pkt.path");
            c.raw_base_path = pt.get<std::string>("data-server.raw_pkt.base_path");

            c.timer_hdf5_flush = pt.get<uint64_t>("data-server.hdf5.flush_timeout");
            c.timer_hdf5_change_file = pt.get<uint64_t>("data-server.hdf5.change_file");
            c.hdf5_path = pt.get<std::string>("data-server.hdf5.path");
            c.hdf5_base_path = pt.get<std::string>("data-server.hdf5.base_path");

            c.timer_hk_scan = pt.get<int64_t>("data-server.timer_hk_scan");
            c.timer_hk_set = pt.get<int64_t>("data-server.timer_hk_set");

            c.timer_cryo_scan = pt.get<int64_t>("cryo.timer_probe_scan");
            c.multiplexer_hysteresis = pt.get<uint32_t>("cryo.multiplexer_hysteresis");

            c.json_ip = pt.get<std::string>("web.ip");
            c.json_slo_port = pt.get<uint16_t>("common.json_slo_port");

            //load cryo configuration
            for (auto &sw : pt.get_child("cryo.probes"))
            {
                for (auto &ch : sw.second)
                {
                    c.cryo_config.push_back({.id = ch.second.get<std::string>("id"),
                                             .cryo_unit = ch.second.get<std::string>("unit"),
                                             .channel = ch.first,
                                             .sw_pos = sw.first,
                                             .calibration = ch.second.get<std::string>("calibration")});
                    c.cryo_map[sw.first][ch.first] = std::prev(c.cryo_config.end());
                }
            }

            //load addresses
            auto l = [&](const char *json_item, auto &addres_list, auto &map_str, auto &map_u8) {
                for (auto &e : pt.get_child(json_item))
                {
                    std::string name = e.second.get<std::string>("name");
                    std::string addr_s = e.second.get<std::string>("addr");
                    std::string type = e.second.get<std::string>("type");
                    addr_item::Type t = type == "u16" ? addr_item::Type::u16 : addr_item::Type::i16;
                    uint8_t addr = std::stoi(addr_s, nullptr, 16);
                    bool ro = e.second.get<bool>("read_only");
                    addres_list.push_back({name, addr, t, ro});
                    map_str[name] = std::prev(addres_list.end());
                    map_u8[addr] = std::prev(addres_list.end());
                }
            };

            l("daq_board_addr.BIAS_POL", c.address_bias_pol, c.map_bias_pol_str, c.map_bias_pol_u8);
            l("daq_board_addr.BIAS_SYS", c.address_bias_sys, c.map_bias_sys_str, c.map_bias_sys_u8);
            l("daq_board_addr.DAQ_POL", c.address_daq_pol, c.map_daq_pol_str, c.map_daq_pol_u8);
            l("daq_board_addr.DAQ_SYS", c.address_daq_sys, c.map_daq_sys_str, c.map_daq_sys_u8);

            c.json_schema_path_slo = pt.get<std::string>("data-server.json_schema_path_slo");
        }

        boost::asio::ip::address resolve_or_fail(const std::string &hostname)
        {
            int max = 50;
            for (auto i = 0; i <= max; i++)
            {
                try
                {
                    boost::asio::ip::udp::resolver resolver(ioService::service);
                    boost::asio::ip::address addr = resolver.resolve({hostname, ""})->endpoint().address();
                    return addr;
                }
                catch (const std::exception &e)
                {
                    std::cout << "error while resolving: " << hostname << std::endl;
                    if (i == max)
                        throw e;
                }
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
        }

        int init()
        {
            int errors = 0;
            std::vector<boost::future<int>> wr;

            Config &c = *singletons.config.get();
            singletons.workers_pool.reset(new ThreadPool(c.workers));
            singletons.unix_sig.reset(new UnixSignals());
            std::signal(SIGINT, handle_signal);
            auto wamp_ip = resolve_or_fail(c.wamp_router_ip);
            //LOG FIRST
            Log::init();
            singletons.db_log.reset(new DbLog(c.db_user, c.db_password, c.db_name, c.db_ip, c.db_port));
            singletons.db_log->connect_log(Log::sig_log());
            // BEGIN singletons
            // DB FIRST
            singletons.db_slo.reset(new DbSloCommand(c.db_user, c.db_password, c.db_name, c.db_ip, c.db_port));
            singletons.db_hdf5.reset(new DbH5info(c.db_user, c.db_password, c.db_name, c.db_ip, c.db_port));

            singletons.timer_hdf5_change_file.reset(new Timer());
            singletons.timer_hdf5_flush.reset(new Timer());
            singletons.timer_raw_change_file.reset(new Timer());
            singletons.timer_raw_flush.reset(new Timer());
            singletons.timer_hk_scan.reset(new Timer());
            singletons.timer_hk_set.reset(new Timer());
            singletons.timer_cryo_scan.reset(new Timer());

            for (const auto &sw : c.cryo_map)
            {
                for (const auto &ch : sw.second)
                {
                    CryoProbe *probe = new CryoProbe(ch.second->id,
                                                     ch.second->cryo_unit,
                                                     ch.second->channel,
                                                     ch.second->sw_pos,
                                                     ch.second->calibration);
                    singletons.cryo_probes[sw.first][ch.first].reset(probe);
                    try
                    {
                        probe->init();
                    }
                    catch (const std::exception &e)
                    {
                        Log::log(utils::LogMessage::Level::ERROR, "INIT", "error while initializing cryo probe: " + std::string(e.what()));
                        std::cout << e.what() << std::endl;
                        errors++;
                    }
                }
            }

            boost::asio::ip::address web_addr = resolve_or_fail(c.json_ip);
            singletons.tag_control.reset(new TagControl(c.db_user, c.db_password, c.db_name, c.db_ip, c.db_port));
            singletons.wamp_log.reset(new autobahn::Log(wamp_ip, c.wamp_router_port));
            wr.emplace_back(singletons.wamp_log->init(c.wamp_realm));
            singletons.wamp_tag.reset(new autobahn::Tag(wamp_ip, c.wamp_router_port));
            wr.emplace_back(singletons.wamp_tag->init(c.wamp_realm));

            for (const auto &b : c.daq_name)
            {
                boost::asio::ip::address addr;
                addr = resolve_or_fail(b.second->sci_ip);
                singletons.daq_udp_servers[b.first].reset(new DaqUdpServer(addr, b.second->sci_port));
            }

            for (auto &b : c.daq_name)
            {
                auto fs = new DaqFileStream();
                singletons.daq_file_streams[b.first].reset(fs);
                fs->base_path(c.raw_base_path);
                fs->create(c.raw_path + "_DAQ_" + b.first);
                singletons.hk_streams[b.first].reset(new HkStream(b.first));
                singletons.wamp_daq_pkt[b.first].reset(new autobahn::DaqPkt(wamp_ip, c.wamp_router_port));
                wr.emplace_back(singletons.wamp_daq_pkt[b.first]->init(c.wamp_realm));
            }
            singletons.wamp_hk_stream.reset(new autobahn::HkStream(wamp_ip, c.wamp_router_port));
            wr.emplace_back(singletons.wamp_hk_stream->init(c.wamp_realm));

            singletons.wamp_snapshot.reset(new autobahn::Snapshot());
            singletons.hdf5_stream_sci.reset(new H5Stream());
            singletons.hdf5_stream_sci->base_path(c.hdf5_base_path);
            // first connect to db oblect in order to create entry for first new file
            singletons.db_hdf5->connect_h5info(singletons.hdf5_stream_sci->sig_file());
            singletons.hdf5_stream_sci->create(c.hdf5_path);

            for (const auto &b : c.daq_name)
            {
                boost::asio::ip::address addr;
                addr = resolve_or_fail(b.second->slo_ip);
                singletons.slo_boards[b.first].reset(new SloControl(b.first, addr, b.second->slo_port));
            }

            boost::asio::ip::address addr;
            //TODO    addr = resolve_or_fail(c.cryo.first);
            //TODO    singletons.cryo_unit.reset(new SloControl(addr,c.cryo.second));
            std::cout << "BEGIN ------------------------------------------------------ CRYO" << std::endl;
            for (auto &cu : c.cryo)
            {
                std::cout << "CRYO " << cu.first << std::endl;
                boost::system::error_code ec;
                if (cu.second.get<std::string>("connection.type", "NULL") == "TCP")
                {
                    singletons.cryo_units[cu.first].reset(new LakeshoreTCP());
                    singletons.cryo_units[cu.first]->set_config(cu.second);
                    singletons.cryo_units[cu.first]->start(ec);
                    if (ec)
                    {
                        std::cout << "error " << cu.first << ": " << ec.message() << std::endl;
                    }
                    else
                    {
                        std::cout << cu.first << " started" << std::endl;
                    }
                }
                else if (cu.second.get<std::string>("connection.type", "NULL") == "MUX")
                {
                    singletons.cryo_units[cu.first].reset(new Multiplexer());
                    singletons.cryo_units[cu.first]->set_config(cu.second);
                    singletons.cryo_units[cu.first]->start(ec);
                    if (ec)
                    {
                        std::cout << "error " << cu.first << ": " << ec.message() << std::endl;
                    }
                    else
                    {
                        std::cout << cu.first << " started" << std::endl;
                    }
                }
                else
                {
                    std::cout << "ERROR config for " << cu.first << std::endl;
                }
            }
            singletons.cryo_stream.reset(new CryoStream(c.multiplexer_hysteresis));
            std::cout << "END ------------------------------------------------------ CRYO" << std::endl;

            if (JsonControl::init() != 0) // error loading json schemas for input validation, exit
                return 1;

            singletons.json_server.reset(new ServerTCP<JsonControl>(c.json_slo_port));
            singletons.af_dispatcher.reset(new AutoDispatcher());

            // END singletons

            // WAIT RESULTS FROM WAMP INIT
            /* for (auto& f : wr){
        int i = f.get();
        if(i) return i; //cannot connect to crossbar, return error;
    }
    */
            return errors;
        } // namespace strip

        void connect_signals()
        {
            // BEGIN SIGNAL CONNECTIONS
            Config &c = *singletons.config.get();
            singletons.wamp_log->connect_log(Log::sig_log());
            singletons.wamp_tag->connect_tag(singletons.tag_control->sig_tag());

            for (auto &b : singletons.daq_file_streams)
            {
                b.second->connect_pkt(singletons.daq_udp_servers.at(b.first)->sig_pkt());
                b.second->connect_pol_flag(singletons.slo_boards.at(b.first)->sig_flag());
                //TODO b.second->connect_tel_flag();
                b.second->connect_tag(singletons.tag_control->sig_tag());
                b.second->connect_change_file(singletons.timer_raw_change_file->sig_timer());
                b.second->connect_flush(singletons.timer_raw_flush->sig_timer());

                singletons.af_dispatcher->connect_pkt(singletons.daq_udp_servers.at(b.first)->sig_pkt());
                singletons.af_dispatcher->connect_pol_flag(singletons.slo_boards.at(b.first)->sig_flag());

                singletons.hdf5_stream_sci->connect_pol_flag(singletons.slo_boards.at(b.first)->sig_flag());
            }

            for (auto &d : singletons.daq_udp_servers)
            {
                singletons.hdf5_stream_sci->connect_pkt(d.second->sig_pkt());
                singletons.wamp_daq_pkt[d.first]->connect_pkt(d.second->sig_pkt());
                singletons.wamp_snapshot->connect_pkt(d.second->sig_pkt());
            }

            //TODO singletons.hdf5_stream_sci->connect_tel_flag();
            singletons.hdf5_stream_sci->connect_tag(singletons.tag_control->sig_tag());
            singletons.hdf5_stream_sci->connect_change_file(singletons.timer_hdf5_change_file->sig_timer());
            singletons.hdf5_stream_sci->connect_flush(singletons.timer_hdf5_flush->sig_timer());
            singletons.hdf5_stream_sci->connect_log(Log::sig_log());
            singletons.hdf5_stream_sci->connect_cryo(singletons.cryo_stream->sig_cryo_pkt());

            for (auto &b : singletons.slo_boards)
            {
                singletons.db_slo->connect_req(b.second->sig_req());
                singletons.db_slo->connect_ack(b.second->sig_ack());

                singletons.hdf5_stream_sci->connect_req(b.second->sig_req());
                singletons.hdf5_stream_sci->connect_ack(b.second->sig_ack());
            }
            //TODO remove this when change cryo class
            //    singletons.db_slo->connect_req(singletons.cryo_unit->sig_req());
            //    singletons.db_slo->connect_ack(singletons.cryo_unit->sig_ack());

            //TODO singletons.af_dispatcher->connect_tel_flag()
            for (auto &p : singletons.hk_streams)
            {
                singletons.slo_boards.at(p.first)->set_hk_stream(p.second);
                p.second->connect_hk(singletons.timer_hk_scan->sig_timer());
                p.second->connect_set(singletons.timer_hk_set->sig_timer());
                singletons.wamp_hk_stream->connect_hk(p.second->sig_hk_pkt());
                singletons.hdf5_stream_sci->connect_hk(p.second->sig_hk_pkt());
            }

            singletons.cryo_stream->connect(singletons.timer_cryo_scan->sig_timer());

            // END SIGNAL CONNECTIONS

            // STARTING timers
            singletons.timer_raw_change_file->start(c.timer_raw_change_file);
            singletons.timer_raw_flush->start(c.timer_raw_flush);

            singletons.timer_hdf5_change_file->start(c.timer_hdf5_change_file);
            singletons.timer_hdf5_flush->start(c.timer_hdf5_flush);

            //fire hks, then start timers
            singletons.timer_hk_scan->force_timeout();
            singletons.timer_hk_set->force_timeout();

            if (c.timer_hk_scan > 0) // timer set to 0 or negative means no HK scan performed
                singletons.timer_hk_scan->start(c.timer_hk_scan);

            if (c.timer_hk_set > 0) // timer set to 0 or negative means no HK set scan performed
                singletons.timer_hk_set->start(c.timer_hk_set);

            if (c.timer_cryo_scan > 0) // timer set to 0 or negative means no HK set scan performed
                singletons.timer_cryo_scan->start(c.timer_cryo_scan);
        }

        signal_halt &UnixSignals::halt3_ext()
        {
            return singletons.unix_sig->_halt3;
        }

        signal_halt &UnixSignals::halt2_ops()
        {
            return singletons.unix_sig->_halt2;
        }

        signal_halt &UnixSignals::halt1_db()
        {
            return singletons.unix_sig->_halt1;
        }

        signal_halt &UnixSignals::halt0_log()
        {
            return singletons.unix_sig->_halt0;
        }

        UnixSignals::UnixSignals() : _halt0(strip::singletons.workers_pool),
                                     _halt1(strip::singletons.workers_pool),
                                     _halt2(strip::singletons.workers_pool),
                                     _halt3(strip::singletons.workers_pool)
        {
        }

        Log::Log() : _sig(singletons.workers_pool) {}

        int Log::init()
        {
            // TODO do something meaninful
            _singleton.reset(new Log());
            _singleton->_start = std::chrono::steady_clock::now();
        }

        signal_log &Log::sig_log()
        {
            return _singleton->_sig;
        }

        void Log::log(LogMessage::Level l, const std::string &emit_class, const std::string &message)
        {
            log(l, emit_class, message, "system");
        }

        void Log::log(LogMessage::Level l,
                      const std::string &emit_class,
                      const std::string &message,
                      const std::string &user)
        {
            std::shared_ptr<LogMessage> msg(new LogMessage);

            msg->timestamp = std::chrono::system_clock::now();
            msg->level = l;
            msg->emit_class = emit_class;
            msg->message = message;
            msg->user = user;

            _singleton->_sig(msg);
        }

        constexpr double H_DAY = 24.0;
        constexpr double MIN_DAY = 1440.0;
        constexpr double SEC_DAY = 86400.0;
        constexpr double MICRO = 1000000.0;
        constexpr uint64_t NANO = 1000000000;

        timeval Time::_start_tv;
        uint32_t Time::_start_pkt = 0;

        void Time::set_offset(uint32_t timestamp, timeval tv)
        {
            _start_tv = tv;
            _start_pkt = timestamp;
        }

        double Time::get_MJD(timeval tv)
        {
            tm tt;
            time_t ts = tv.tv_sec;
            gmtime_r(&ts, &tt);

            int year = tt.tm_year + 1900;
            int month = tt.tm_mon + 1;
            int day = tt.tm_mday;

            double mjd0, mjd1;

            int err = eraCal2jd(year, month, day, &mjd0, &mjd1);
            if (err)
                return 0;

            double hours = tt.tm_hour / H_DAY;
            double min = tt.tm_min / MIN_DAY;
            double sec = tv.tv_usec;
            sec /= MICRO;
            sec += tt.tm_sec;
            sec /= SEC_DAY;

            double mjd = sec + min + hours + mjd1;

            /*
    std::cout << tt.tm_year+1900 << "/" << tt.tm_mon << "/" << tt.tm_mday
              << " " << tt.tm_hour << ":" << tt.tm_min << ":" << tt.tm_sec
              << "." << tv.tv_usec << " " << tt.tm_isdst << std::endl;
*/
            return mjd;
        }

        double Time::get_pkt_MJD(uint32_t timestamp)
        {
            uint32_t diff = timestamp - _start_pkt;
            uint32_t sec = diff / 100;
            uint32_t csec = diff % 100;
            uint64_t usec = csec * 10000;
            usec += _start_tv.tv_usec;
            sec += usec / 1000000;
            usec = usec % 1000000;

            timeval tv;
            tv.tv_sec = _start_tv.tv_sec + sec;
            tv.tv_usec = usec;

            return get_MJD(tv);
        }

        double Time::get_unix_MJD(uint64_t timestamp_nano)
        {
            timeval tv;
            tv.tv_sec = timestamp_nano / NANO;
            tv.tv_usec = (timestamp_nano % NANO) / 1000.0; // nano to micro
            return get_MJD(tv);
        }

        double Time::get_now_MJD()
        {
            timeval tv;
            gettimeofday(&tv, nullptr);
            return get_MJD(tv);
        }

    } // namespace utils
} // namespace strip
