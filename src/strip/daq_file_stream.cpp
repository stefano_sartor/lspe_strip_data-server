#include "../../include/strip/daq_file_stream.hpp"
#include <time.h>
#include <libgen.h>
#include <iostream>
#include <sstream>
#include <experimental/filesystem>
#include <system_error>

#include "../../include/strip/utils.hpp"

namespace fs = std::experimental::filesystem;

namespace strip {

DaqFileStream::DaqFileStream(){
    ext = ".bin";
    class_name = "DaqFileStream";
}

void DaqFileStream::imp_close(){
    /****************************
    size_t maxq = 0;
    for(signal_pkt::Connection& c : _conn_pkt){
        maxq = std::max(maxq,c->max_queue);
        c->max_queue=0;
    }

    utils::Log::log(utils::LogMessage::Level::INFO,
                    class_name,
                    "MAXQ RAW: "+std::to_string(maxq));

    //**************************/
    _fs.close();
}

bool DaqFileStream::imp_open(const std::string & file_path){
    _fs.open(file_path,std::fstream::out | std::fstream::binary);
    return ! _fs.fail();
}

void DaqFileStream::imp_flush(){
    _fs.flush();
}



void DaqFileStream::imp_add(std::shared_ptr<const DaqUdpPkt>& pkt){
    _fs.write(pkt->buffer,strip::DAQ_UDP_LEN);
}

}
