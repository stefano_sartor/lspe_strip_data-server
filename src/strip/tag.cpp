#include "../../include/strip/tag.hpp"
#include "../../include/strip/utils.hpp"
#include "../../include/strip/singleton.hpp"
#include "../../odb/tag_data-odb.hxx"
#include <odb/query.hxx>
namespace strip {


const std::string TagControl::class_name = "TagControl";
TagControl::TagControl(const std::string &user, const std::string &password, const std::string &db_name, const std::string &host, uint16_t port) :
    _sig(singletons.workers_pool)
{
    _db.reset(new odb::mysql::database (user,password,db_name,host,port));
}

typedef odb::query<DbTag> query;
typedef odb::result<DbTag> result;

TagControl::Status TagControl::tag(Tag &tg){
    tg.mjd = utils::Time::get_now_MJD();
    Status ret = Status::OK;

    std::string tag = tg.tag;
    std::cout << "*** *** BEGIN TAG TRANSACTION" << std::endl;
    try{
        odb::transaction t (_db->begin ());
        query q(query::tag == tag && query::mjd_stop < 0);
        result r = _db->query<DbTag>(q);
        if(tg.type == Tag::Type::START){
            if(r.size() > 0){
                ret = Status::ALREADY_STARTED;
            }else{
                DbTag d;
                d.tag = tag;
                d.mjd_start = tg.mjd;
                d.comment_start = tg.comment;
                d.mjd_stop = -1;
                _db->persist(d);
                ret = Status::OK;
                _sig(std::shared_ptr<const DbTag>(new DbTag(d)));
            }
        }else{
            if(r.size() == 0){
                ret = Status::UNKNOWN;
            }else{// should be 1 the result size
                DbTag d = *r.begin();
                d.comment_stop = tg.comment;
                d.mjd_stop = tg.mjd;
                _db->update(d);
                ret = Status::OK;
                _sig(std::shared_ptr<const DbTag>(new DbTag(d)));
            }
        }
        t.commit();
    }catch(const odb::exception& e){
        utils::Log::log(utils::LogMessage::ERROR,class_name,e.what());
        ret = Status::UNKNOWN;
    }
    std::cout << "*** *** END TAG TRANSACTION" << std::endl;


    return ret;
}

std::vector<std::shared_ptr<const DbTag>> TagControl::get_active(){
  std::vector<std::shared_ptr<const DbTag>> v;
  try{
        odb::transaction t (_db->begin ());
        query q(query::mjd_stop < 0);
        result r = _db->query<DbTag>(q);
        for( auto& tag : r){
          v.push_back(std::shared_ptr<const DbTag>(new DbTag(tag)));
        }

      }catch(const odb::exception& e){
        utils::Log::log(utils::LogMessage::ERROR,class_name,e.what());
      }

      return v;
}

}
