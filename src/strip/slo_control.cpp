#include "../../include/strip/slo_control.hpp"
#include "../../include/strip/utils.hpp"
#include "../../include/strip/singleton.hpp"
#include "../../include/strip/hk_stream.hpp"

#include <sstream>
#include <iomanip>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/select.h>
#include <stdio.h>
#include <string.h>

#include <iostream> //DBG

using boost::asio::ip::udp;

namespace strip
{

    const std::string SloControl::class_name = "SloControl";
    const char SloControl::encode_counter[SloControl::COUNTER_BASE] =
        {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
         'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
         'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
         'U', 'V', 'W', 'X', 'Y', 'Z'};

    /*
void SloControl::init(const vec_endpoint_t &boards, const endpoint_t &cryo){
    _boards.clear();

    for(const endpoint_t &e : boards){
        std::unique_ptr<SloControl> ptr(new SloControl(e.first,e.second));
        SloControl::_boards.push_back(std::move(ptr));
    }

    if(cryo.second != 0){
        SloControl::_cryo.reset(new SloControl(cryo.first,cryo.second));
    }
}

SloControl* SloControl::get_board(uint8_t board_no){
    if(board_no >= SloControl::_boards.size()){
        return nullptr;
    }else{
        return SloControl::_boards[board_no].get();
    }
}

SloControl* SloControl::get_cryo(){
    return SloControl::_cryo.get();
}
*/
    SloControl::SloControl(const std::string &name, const boost::asio::ip::address &addr, uint16_t port)
        : _board_name(name),
          _counter(0),
          _n_timeouts(0),
          _remote_endpoint(addr, port),
          _socket(strip::utils::ioService::service, udp::endpoint(boost::asio::ip::address::from_string("0.0.0.0"), port)), // bind to any address
          _sig_flag(singletons.workers_pool),
          _sig_req(singletons.workers_pool),
          _sig_ack(singletons.workers_pool)
    //_socket(strip::utils::ioService::service, udp::endpoint(udp::v4(), port))
    {
        _conn_close = utils::UnixSignals::halt3_ext().connect(std::bind(&SloControl::slot_close, this));
    }

    boost::system::error_code SloControl::finalize_command(boost::system::error_code err_code, SloCommand &req, bool sig)
    {
        if (err_code)
        { //got an error, log it
            utils::Log::log(utils::LogMessage::Level::ERROR, class_name, err_code.message());
        }
        std::shared_ptr<SloCommand> msg(new SloCommand(req));
        if (sig)
            _sig_ack(msg);
        return err_code;
    }

    boost::system::error_code SloControl::send_command(SloCommand &req, uint16_t millis, bool sig)
    {
        if (req.method == SloCommand::Method::GET)
            return do_send_command(req, millis, sig);

        SloCommand check = req;
        boost::system::error_code set_res = do_send_command(req, millis, sig);
        if (set_res) // some error -> return it
            return set_res;
        else
        {
            /* capture HK_SCAN command:
             * if the user sets HK_SCAN with 23295, we shoul read *_HK and log them in hk_stream
             */
            if (check.base_addr == "HK_SCAN" &&
                check.data.size() == 1 && check.data[0] == 0x5AFF &&
                _hk_stream && // if _hk_stream must be valid
                sig           // we use sig flag to dscriminate commands from user or HKStream objs
            )
            {
                std::cout << "+++++++++++++ HK_SCAN trigger +++++++++++++" << std::endl;
                std::chrono::system_clock::time_point hk_ts = check.timestamp;

                std::this_thread::sleep_for(std::chrono::milliseconds(100));

                req.data[0] = 0x5AFF;
                _hk_stream->read_hk(hk_ts, req.data[0], 10);
                return set_res;
            }

            req.method = SloCommand::Method::GET;
            set_res = do_send_command(req, millis, sig);
            req.method = SloCommand::Method::SET;

            if (sig && _hk_stream)
                _hk_stream->send_sig(req);

            if (set_res)
            { // some error -> return it
                return set_res;
            }
            else
            { //check if data is equal
                if (req.data.size() != check.data.size())
                {
                    req.res = SloCommand::Result::ERROR_DATA;
                    return boost::system::error_code(boost::system::errc::bad_message, boost::system::system_category());
                }
                /*  2019_03_28: some registers quickly resets themselves after a set command
                 * i.e. HK_SCAN changes the biflag as it scans the HK values
                 * therefore set data may differ from a get data even if oly a few millis have been past.
                 * Check of the data just set may result in false error, ence it is better to return the read to the user
                 */
                /*else{
                    for(auto i=0; i<req.data.size();i++){
                        if(req.data[i] != check.data[i]){
                            //DBG std::cout << "req["<<i<<"]=" <<req.data[i] << " check["<<i<<"]="<<check.data[i] <<std::endl;
                            req.res = SloCommand::Result::ERROR_DATA;
                            return boost::system::error_code(boost::system::errc::bad_message,boost::system::system_category());
                        }
                    }
                }*/
            }

            return set_res;
        }
    }

    boost::system::error_code SloControl::do_send_command(SloCommand &req, uint16_t millis, bool sig)
    {
        if (_n_timeouts >= MAX_TIMEOUTS)
        { // we received MAX_TIMEOUTS consecutive timeouts, change connection
            utils::Log::log(utils::LogMessage::Level::INFO, class_name, "BOARD '" + _board_name + "': MAX_TIMEOUTS reached. Reopening connection");
            boost::system::error_code ec;
            _socket.close(ec);
            if (ec)
            {
                utils::Log::log(utils::LogMessage::Level::ERROR, class_name, "CLOSING CONNECTION: " + ec.message());
            }
            _socket = boost::asio::ip::udp::socket(strip::utils::ioService::service,
                                                   udp::endpoint(boost::asio::ip::address::from_string("0.0.0.0"),
                                                                 _remote_endpoint.port())); // bind to any address
        }

        req.counter = _counter++;
        _counter = _counter % COUNTER_BASE;
        std::string message = encode(req);
        if (req.res != SloCommand::Result::OK)
        {
            req.res = SloCommand::Result::ERROR;
            return finalize_command(boost::asio::error::fault, req, sig);
        }

        std::unique_lock<std::mutex> l(_cmd_sync);
        req.timestamp = std::chrono::system_clock::now();
        //emit request signal
        std::shared_ptr<SloCommand> msg(new SloCommand(req));
        if (sig)
        {
            _sig_req(msg);

            std::string pp = message.substr(0, message.length() - 1);
            std::cout << "SEND $ " << pp << " $" << std::endl
                      << std::flush;
        }
        emit_flag(req);
        _socket.send_to(boost::asio::buffer(message), _remote_endpoint, 0, _error);
        if (_error)
        {
            //std::cout << "error to send" << std::endl; //DBG
            req.res = SloCommand::Result::ERROR_COMM;
            return finalize_command(_error, req, sig);
        }

        auto s = _socket.native_handle();
        fd_set set;
        struct timeval timeout;
        FD_ZERO(&set);   /* clear the set */
        FD_SET(s, &set); /* add our file descriptor to the set */
        timeout.tv_sec = millis / 1000;
        timeout.tv_usec = (millis % 1000) * 1000;

        while (true)
        {
            int rv = select(s + 1, &set, NULL, NULL, &timeout);
            req.timestamp = std::chrono::system_clock::now();
            if (rv == -1)
            { // error occured
                req.res = SloCommand::Result::ERROR_COMM;
                //std::cout << "ERROR SELECT" << std::endl; //DBG
                return finalize_command(boost::system::error_code(errno, boost::system::system_category()), req, sig);
            }
            if (rv == 0)
            {
                _n_timeouts++;
                switch (req.method)
                {
                case SloCommand::Method::GET:
                    req.res = SloCommand::Result::ERROR_TIMEOUT_GET;
                    break;
                case SloCommand::Method::SET:
                    req.res = SloCommand::Result::ERROR_TIMEOUT_SET;
                    break;

                default:
                    req.res = SloCommand::Result::ERROR_COMM;
                    break;
                }

                //std::cout << "ERROR TIMEOUT" << std::endl; //DBG
                return finalize_command(boost::asio::error::would_block, req, sig);
            }
            _n_timeouts = 0; // not timeout, so reset counter

            memset(_buffer, '\0', BUFFER_SIZE);
            socklen_t addr_len = _remote_endpoint.size();
            ssize_t recv_size = recvfrom(s, _buffer, BUFFER_SIZE, 0, _remote_endpoint.data(), &addr_len);
            if (recv_size == -1)
            { // error occured
                req.res = SloCommand::Result::ERROR_COMM;
                //std::cout << "ERROR RECEIVE" << std::endl; //DBG
                return finalize_command(boost::system::error_code(errno, boost::system::system_category()), req, sig);
            }

            SloCommand r = decode(std::string(_buffer));
            r.timestamp = req.timestamp;

            if (sig)
            {
                std::string pp = r.message.substr(0, r.message.length() - 1);
                std::cout << "RESP $ " << pp << " $" << std::endl
                          << std::flush;
            }

            if (is_response(req, r))
            {
                clear_flag(r);

                if (r.res != SloCommand::Result::OK)
                {
                    //std::cout << "ERROR RETURNDED FROM SLO" << std::endl; //DBG
                    return finalize_command(boost::asio::error::fault, req, sig);
                }
                req = r;
                return finalize_command(boost::system::error_code(0, boost::system::generic_category()), req, sig);
            }
            else
            {
                utils::Log::log(utils::LogMessage::Level::WARNING, class_name, "SLO command out of sync");
            }
        }
    }

    std::string SloControl::encode(SloCommand &req)
    {
        std::stringstream s;

        auto &map_bias_pol = singletons.config->map_bias_pol_str;
        auto &map_bias_sys = singletons.config->map_bias_sys_str;
        auto &map_daq_pol = singletons.config->map_daq_pol_str;
        auto &map_daq_sys = singletons.config->map_daq_sys_str;

        s << "#" << std::dec;
        switch (req.type)
        {
        case SloCommand::Type::BIAS:
            s << "B";
            if (req.pol_no < 8)
            { // BIAS_POL table
                s << int(req.pol_no);
                if (map_bias_pol.find(req.base_addr) != map_bias_pol.end())
                {
                    s << std::setfill('0') << std::setw(2) << std::hex << std::uppercase << int(map_bias_pol.at(req.base_addr)->address);
                }
                else
                {
                    req.res = SloCommand::Result::BAD_ADDRESS;
                    return "";
                }
            }
            else if (req.pol_no == BOARD_ADDR)
            { // BIAS_SYS table
                s << "F";
                if (map_bias_sys.find(req.base_addr) != map_bias_sys.end())
                {
                    s << std::setfill('0') << std::setw(2) << std::hex << std::uppercase << int(map_bias_sys.at(req.base_addr)->address);
                }
                else
                {
                    req.res = SloCommand::Result::BAD_ADDRESS;
                    return "";
                }
            }
            else
            {
                req.res = SloCommand::Result::BAD_TYPE;
                return "";
            }
            break;
        case SloCommand::Type::DAQ:
            s << "P";
            if (req.pol_no < 8)
            { // DAQ_POL table
                s << int(req.pol_no);
                if (map_daq_pol.find(req.base_addr) != map_daq_pol.end())
                {
                    s << std::setfill('0') << std::setw(2) << std::hex << std::uppercase << int(map_daq_pol.at(req.base_addr)->address);
                }
                else
                {
                    req.res = SloCommand::Result::BAD_ADDRESS;
                    return "";
                }
            }
            else if (req.pol_no == BOARD_ADDR)
            { //DAQ_SYS table
                s << "F";
                if (map_bias_sys.find(req.base_addr) != map_daq_sys.end())
                {
                    s << std::setfill('0') << std::setw(2) << std::hex << std::uppercase << int(map_daq_sys.at(req.base_addr)->address);
                }
                else
                {
                    req.res = SloCommand::Result::BAD_ADDRESS;
                    return "";
                }
            }
            else
            {
                req.res = SloCommand::Result::BAD_TYPE;
                return "";
            }
            break;
        case SloCommand::Type::CRYO:
            // TODO waiting for specification
            req.res = SloCommand::Result::BAD_TYPE;
            return "";
        default:
            break;
        }

        s << std::hex << std::uppercase;

        if (req.method == SloCommand::Method::GET)
        {
            s << "?";
            if (!req.data.empty())
                s << req.data.size();
        }
        else
        {
            if (req.data.empty())
            {
                req.res = SloCommand::Result::ERROR_SYNTAX;
                return "";
            }

            //TODO emit change settigs signal for flag settings
            //TODO check readonly write
            s << "=";
            s << req.data[0];
            for (auto i = 1; i < req.data.size(); i++)
            {
                s << "," << req.data[i];
            }
        }
        s << '|' << encode_counter[req.counter];
        s << '\r';
        req.message = s.str();
        return s.str();
    }

    SloCommand SloControl::decode(std::string response)
    {
        SloCommand req;
        req.method = SloCommand::Method::NONE;
        req.type = SloCommand::Type::NONE;
        req.pol_no = NULL_ADDR;
        req.message = response;
        req.board_name = _board_name;
        req.counter = -1;

        auto &map_bias_pol = singletons.config->map_bias_pol_u8;
        auto &map_bias_sys = singletons.config->map_bias_sys_u8;
        auto &map_daq_pol = singletons.config->map_daq_pol_u8;
        auto &map_daq_sys = singletons.config->map_daq_sys_u8;

        size_t p = response.find('\r');
        if (p == std::string::npos)
        {
            req.res = SloCommand::Result::ERROR;
            std::cout << "NO final \\r" << std::endl; //DBG
            return req;
        }

        p = response.find('|');
        if (p != std::string::npos)
        {
            try
            {
                req.counter = std::stoi(response.substr(p + 1), nullptr, COUNTER_BASE);
            }
            catch (const std::exception &e)
            {
                utils::Log::log(utils::LogMessage::Level::ERROR, class_name, "bad response from SLO command [" + response + "]:" + e.what());
                req.res = SloCommand::Result::ERROR_DECODE;
                return req;
            }
        }

        p = response.find("UNKNOWN_COMMAND");
        if (p != std::string::npos)
        {
            req.res = SloCommand::Result::UNKNOWN_COMMAND;
            std::cout << "UNKNOWN_COMMAND" << std::endl; //DBG
            return req;
        }

        if (response[1] == 'B')
        {
            req.type = SloCommand::Type::BIAS;
        }
        else if (response[1] == 'P')
        {
            req.type = SloCommand::Type::DAQ;
        }

        p = response.find("ROR");
        if (p != std::string::npos)
        {
            req.res = SloCommand::Result::ERROR_RO;
            std::cout << "RO" << std::endl; //DBG
            req.method = SloCommand::Method::SET;
            return req;
        }

        if (response.find("TLM") != std::string::npos)
        {
            req.method = SloCommand::Method::GET;
        }
        else if (response.find("OK") != std::string::npos)
        {
            req.method = SloCommand::Method::SET;
        }

        p = response.find(":");
        p++;
        try
        {
            req.pol_no = std::stoi(response.substr(p), nullptr, 16);
        }
        catch (const std::exception &e)
        {
            utils::Log::log(utils::LogMessage::Level::ERROR, class_name, "bad response from SLO command [" + response + "]:" + e.what());
            req.res = SloCommand::Result::ERROR_DECODE;
            return req;
        }

        std::map<uint8_t, std::list<addr_item>::const_iterator> *map = nullptr;
        if (req.type == SloCommand::Type::BIAS)
        {
            if (req.pol_no < 8)
                map = &map_bias_pol;
            else
                map = &map_bias_sys;
        }
        else if (req.type == SloCommand::Type::DAQ)
        {
            if (req.pol_no < 8)
                map = &map_daq_pol;
            else
                map = &map_daq_sys;
        }
        else
        {
            // TODO waiting for specification
            req.res = SloCommand::Result::BAD_TYPE;
            return req;
        }
        p = response.find(";");
        response = response.substr(p + 1);
        uint8_t base_address;
        try
        {
            base_address = std::stoi(response, nullptr, 16);
        }
        catch (const std::exception &e)
        {
            utils::Log::log(utils::LogMessage::Level::ERROR, class_name, "bad response from SLO command [" + response + "]:" + e.what());
            req.res = SloCommand::Result::ERROR_DECODE;
            return req;
        }

        if (map->find(base_address) == map->end())
        {
            std::cout << "base_addr not found: " << std::hex << std::uppercase << (int)base_address << std::endl;
            req.res = SloCommand::Result::ERROR;
            return req;
        }
        else
        {
            req.base_addr = map->at(base_address)->name;
        }

        if (req.method == SloCommand::Method::GET)
        {
            p = response.find("=");
            response = response.substr(p + 1);
            //std::cout <<"data:[";//DBG
            while (p != std::string::npos)
            {
                try
                {
                    req.data.push_back(std::stol(response, nullptr, 16));
                    //std::cout << req.data.back() << ",";
                }
                catch (const std::exception &e)
                {
                    utils::Log::log(utils::LogMessage::Level::ERROR, class_name, "bad response from SLO command [" + response + "]:" + e.what());
                    req.res = SloCommand::Result::ERROR_DECODE;
                    return req;
                }

                p = response.find(",");
                response = response.substr(p + 1);
            }
            //std::cout <<"]" << std::endl;
            req.res = SloCommand::Result::OK;
        }
        else if (req.method == SloCommand::Method::SET)
        {
            p = response.find(";");
            response = response.substr(p + 1);
            int i;
            try
            {
                i = std::stoi(response, nullptr, 16);
            }
            catch (const std::exception &e)
            {
                utils::Log::log(utils::LogMessage::Level::ERROR, class_name, "bad response from SLO command [" + response + "]:" + e.what());
                req.res = SloCommand::Result::ERROR_DECODE;
                return req;
            }

            req.data.assign(i, 0);
            req.res = SloCommand::Result::OK;
        }

        return req;
    }

    bool SloControl::is_response(const SloCommand &req, const SloCommand &recv)
    {
        if (req.board_name != recv.board_name || //redoundant
            req.pol_no != recv.pol_no ||
            req.method != recv.method ||
            req.type != recv.type ||
            req.base_addr != recv.base_addr ||
            req.data.size() != recv.data.size() ||
            (recv.counter > 0 && req.counter != recv.counter))
            return false;
        else
            return true;
    }

    void SloControl::emit_flag(SloCommand &req)
    {
        if (req.method == SloCommand::Method::GET)
            return;
        if (req.type == SloCommand::Type::CRYO)
            return;

        std::shared_ptr<PolarimeterFlag> f(new PolarimeterFlag());
        f->board_name = req.board_name;
        f->pol_no = 0xF; //TODO discriminate the polarimeter
        f->flag = PolarimeterFlag::Flag::INVALID;

        _sig_flag(f);
    }

    void SloControl::clear_flag(SloCommand &req)
    {
        if (req.method == SloCommand::Method::GET)
            return;
        if (req.type == SloCommand::Type::CRYO)
            return;

        std::shared_ptr<PolarimeterFlag> f(new PolarimeterFlag());
        f->board_name = req.board_name;
        f->pol_no = 0xF; //TODO discriminate the polarimeter
        f->flag = PolarimeterFlag::Flag::INVALID;
        f->dual();

        _sig_flag(f);
    }

    int SloControl::slot_close()
    {
        _socket.close();
        _conn_close.reset();
    }

} // namespace strip
