#include "../../include/wamp/snapshot.hpp"
#include "../../include/autobahn/snapshot.hpp"
#include "../../include/strip/singleton.hpp"
//#include "autobahn_upkt.hpp"
#include <boost/variant/get.hpp>
#include <stdexcept>
#include <deque>

namespace strip {
namespace autobahn{

const std::string SnapshotWamp::class_name = "autobahn::SnapshotWamp";

SnapshotWamp::SnapshotWamp(const boost::asio::ip::address& addr,uint16_t port) :
    Base(addr,port)
{
    namespace ipc = boost::interprocess;

    const Config& conf = *singletons.config;

    _segment = ipc::managed_shared_memory(ipc::open_only,Snapshot::sh_mem_name.c_str());
    sh_allocator alloc_inst (_segment.get_segment_manager());

    auto sh_pair = _segment.find<sh_vector>(Snapshot::sh_vec_name.c_str());
    if(sh_pair.first == nullptr){
        std::cerr << Snapshot::sh_vec_name <<" shared object not found, exit" << std::endl;
        throw std::runtime_error("shared memory object not found");
    }else{
        _sh_vec.reset(sh_pair.first);
    }

    for(value_pod& pod : *_sh_vec){
        _snapshot[pod.board][pod.pol][pod.table][pod.addr]=&pod;
    }
}

boost::future<int> SnapshotWamp::init(const std::string &realm){
    boost::future<int> future_register = Base::init(realm).then([this](boost::future<int> connected) ->int{
            try{
                int is_ok = connected.get(); //wait for connection and realm join
                 std::unique_lock<std::mutex> l(_m); // lock the connection
                if(is_ok !=0 )return  is_ok;
                boost::future<::autobahn::wamp_registration> registration= _session->provide("strip.snapshot", std::bind(&SnapshotWamp::snapshot, std::ref(*this),std::placeholders::_1));
                _registration = registration.get();
                std::cerr << "registered procedure:" << _registration.id() << std::endl;
            } catch (const std::exception& e) {
                std::cerr << e.what() << std::endl;
                return 1;
            }
            return 0;
    });
    return future_register;
}

void SnapshotWamp::leave(){
    boost::future<void> unprovide = _session->unprovide(_registration);
    try{
        unprovide.get();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
    Base::leave();
}

void SnapshotWamp::snapshot(::autobahn::wamp_invocation invocation){
    arg_map map = invocation->kw_arguments<arg_map>();
    result_map res;
    uint32_t errs = 0;
    for(auto& board : map){
        for(auto& pol: board.second){
            for(auto& table: pol.second){
                for(auto& addr : table.second){
                    try {
                        value v = _snapshot.at(board.first).at(pol.first).at(table.first).at(addr.c_str())->val;
                        res[board.first][pol.first][table.first][addr.c_str()]=v;
                    }catch (const std::out_of_range& oor) {
                        std::cerr << board.first << "."
                                  << pol.first   << "."
                                  << table.first << "."
                                  << addr        << ": "
                                  << oor.what() << std::endl;
                        ++errs;
                    }
                }
            }
        }
    }
    invocation->result(std::make_tuple(errs),res);
}

}
}

#include <msgpack.hpp>

namespace msgpack {
MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
namespace adaptor {

struct packer_imp : public boost::static_visitor<void> {
    void operator()(const uint16_t& v){
        _o.type = msgpack::type::POSITIVE_INTEGER;
        _o.via.u64 = v;
    }

    void operator()(const int16_t& v){
        _o.type = msgpack::type::NEGATIVE_INTEGER;
        _o.via.i64 = v;
    }

    void operator()(const float& v){
        _o.type = msgpack::type::NEGATIVE_INTEGER;
        _o.via.i64 = v;
    }

    packer_imp(msgpack::object::with_zone& o):_o(o) {}
    msgpack::object::with_zone& _o;
};

template <>
struct object_with_zone<strip::autobahn::SnapshotWamp::value> {
    void operator()(msgpack::object::with_zone& o, strip::autobahn::SnapshotWamp::value const& v) const {
        packer_imp imp(o);
        boost::apply_visitor(imp, v);
    }
};


} // namespace adaptor
} // MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS)
} // namespace msgpack

