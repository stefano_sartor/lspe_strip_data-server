#include <iostream>
#include <thread>
#include "include/strip/singleton.hpp"
#include "include/wamp/snapshot.hpp"

void th_go(){
    strip::utils::ioService::service.run();
    std::cout << "----------------------------------ioService exiting" << std::endl;
}
constexpr int th_num = 1;
int main(){
    std::vector<std::thread> th;

   try{
       strip::utils::parse_config("/etc/strip/config.json");
       //strip::utils::parse_config("/home/stefano/workspace/LSPE/data-server/my_config.json");
   }catch(const std::exception& e){
       std::cerr << e.what() <<std::endl;
       std::cerr << "EXIT due to a configuration error" << std::endl;
       return -1;
   }

    const strip::Config& conf = *strip::singletons.config;
   auto wamp_ip =  strip::utils::resolve_or_fail(conf.wamp_router_ip);
   strip::autobahn::SnapshotWamp wamp(wamp_ip,conf.wamp_router_port);
   wamp.init(conf.wamp_realm);

   for (auto i=0; i<th_num; i++) //start async workers for sockets
       th.push_back(std::thread(th_go));


   for(auto& t : th)
       t.join();

   return 0;
}
