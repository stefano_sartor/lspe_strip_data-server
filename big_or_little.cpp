#include "include/strip/daq_pkt.hpp"
#include <bitset>
#include <boost/endian/conversion.hpp>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <fstream>
#include <iomanip> // std::setw
#include <iostream>

const char *CONCORDI =
#ifdef BOOST_LITTLE_ENDIAN
    "LITTLE";
#else
    "BIG";
#endif

const char *DISCONCORDI =
#ifdef BOOST_BIG_ENDIAN
    "LITTLE";
#else
    "BIG";
#endif

uint32_t check = 0xFFF00000;

int main(int argc, char *argv[]) {
  char buff[strip::DAQ_PKT_LEN];

  std::fstream fs;
  fs.open(argv[1], std::fstream::in);

  //  std::cout << "OPEN " << argv[1] << std::endl;

  const char *dem_s = "000";
  const char *tpw_s = "000";

  int32_t dem_big = 0;
  int32_t dem_lit = 0;
  uint32_t tpw_big = 0;
  uint32_t tpw_lit = 0;

  bool dem_is_big = false;
  bool dem_is_lit = false;
  bool tpw_is_big = false;
  bool tpw_is_lit = false;

  size_t tot_cnt = 0;
  size_t dem_big_cnt = 0;
  size_t dem_lit_cnt = 0;
  size_t dem_bad_cnt = 0;

  size_t tpw_big_cnt = 0;
  size_t tpw_lit_cnt = 0;
  size_t tpw_bad_cnt = 0;

  size_t dem_zero_cnt = 0;
  size_t tpw_zero_cnt = 0;

  while (fs) {
    fs.read(buff, strip::DAQ_PKT_LEN);
    tot_cnt++;

    char *s = buff + strip::DaqPkt::SCI_DATA_0;
    dem_big = boost::endian::big_to_native(*reinterpret_cast<int32_t *>(s));
    dem_lit = boost::endian::little_to_native(*reinterpret_cast<int32_t *>(s));
    s += 4;

    tpw_big = boost::endian::big_to_native(*reinterpret_cast<uint32_t *>(s));
    tpw_lit = boost::endian::little_to_native(*reinterpret_cast<uint32_t *>(s));

    if(dem_big==0){
      dem_zero_cnt++;
    }else{
    dem_is_big = (std::abs(dem_big) & check) == 0;
    dem_is_lit = (std::abs(dem_lit) & check) == 0;
    if (dem_is_big && !dem_is_lit)
      dem_big_cnt++;
    else if (!dem_is_big && dem_is_lit)
      dem_lit_cnt++;
    else if (!dem_is_big && !dem_is_lit)
      dem_bad_cnt++;
    }


    if ( tpw_big == 0){
tpw_zero_cnt++;
    }else{

    tpw_is_big = (tpw_big & check) == 0;
    tpw_is_lit = (tpw_lit & check) == 0;


    if (tpw_is_big && !tpw_is_lit)
      tpw_big_cnt++;
    else if (!tpw_is_big && tpw_is_lit)
      tpw_lit_cnt++;
    else
      tpw_bad_cnt++;
    }
  }

  std::cout << "DEM_BIG," << std::setw(7) << dem_big_cnt 
            << ",  DEM_LIT," << std::setw(7) << dem_lit_cnt
            << ",  DEM_BAD," << std::setw(7) << dem_bad_cnt
            << ",  DEM_ZER," << std::setw(7) << dem_zero_cnt 
            << ",  TPW_BIG," << std::setw(7) << tpw_big_cnt 
            << ",  TPW_LIT," << std::setw(7) << tpw_lit_cnt
            << ",  TPW_BAD," << std::setw(7) << tpw_bad_cnt
            << ",  TPW_ZER," << std::setw(7) << tpw_zero_cnt 
            << ",  TOT,   " << std::setw(7) << tot_cnt << ", " << argv[1] << std::endl;

  return 0;
}