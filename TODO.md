LIST OF THINKS TO BE DONE
=========================

* Slo Command, need to differentiate from slo request and slo command struct
  + slo command struct should contain processed data such as resolved name addresses
  + slo response should contain result, slo request no.
  + inheritance should be used for common members

* review H5 pol_flag

* integrate gps sinchronization signal

* include phaseB in hdf5 files (togheter with time_correlation column)
