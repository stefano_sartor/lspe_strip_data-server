#include "include/strip/timer.hpp"
#include "include/strip/daq_udp_server.hpp"
#include "include/strip/daq_pkt.hpp"
#include "include/strip/daq_file_stream.hpp"
#include "include/strip/hdf5_stream.hpp"
#include "include/db/log.hpp"
#include "include/strip/utils.hpp"
#include "include/strip/slo_control.hpp"
#include "include/strip/daq_pkt.hpp"
#include <iostream>
#include <thread>
#include <sstream>
#include <iomanip> 

#include "include/autobahn/daq_pkt.hpp"
#include "boost/variant.hpp"
#include <iostream>

#include "include/strip/hk_stream.hpp"

class my_visitor : public boost::static_visitor<void>
{
public:
    my_visitor() : _tab(0) {}

    void operator()(const bool &i) const
    {
        std::cout << std::boolalpha << i;
    }

    void operator()(const int64_t &i) const
    {
        std::cout << i;
    }

    void operator()(const uint64_t &i) const
    {
        std::cout << i;
    }

    void operator()(const double &i) const
    {
        std::cout << std::fixed << std::setprecision(10) << i;
    }

    void operator()(const std::string &i) const
    {
        std::cout << i;
    }
    void operator()(const boost::basic_string_ref<char, std::char_traits<char>> &i) const
    {
        std::cout << i;
    }

    void operator()(const std::vector<char> &) const
    {
    }

    void operator()(const msgpack::v1::type::raw_ref &) const
    {
    }

    void operator()(const msgpack::v1::type::ext &) const
    {
    }

    void operator()(const std::vector<msgpack::type::variant> &i) const
    {
        for (const auto &p : i)
        {
            tab();
            _tab++;
            boost::apply_visitor(*this, p);
            std::cout << "," << std::endl;
            _tab--;
        }
    }

    void operator()(const std::map<msgpack::type::variant, msgpack::type::variant> &i) const
    {
        std::cout << std::endl
                  << "map size:" << i.size() << std::endl;
        for (const auto &p : i)
        {

            tab();
            boost::apply_visitor(*this, p.first);
            std::cout << " : ";
            _tab++;
            boost::apply_visitor(*this, p.second);
            std::cout << std::endl;
            _tab--;
        }
    }

    void operator()(const std::multimap<msgpack::type::variant, msgpack::type::variant> &i) const
    {
        for (const auto &p : i)
        {
            tab();
            boost::apply_visitor(*this, p.first);
            std::cout << " : ";
            _tab++;
            boost::apply_visitor(*this, p.second);
            std::cout << std::endl;
            _tab--;
        }
    }

    void operator()(const msgpack::v1::type::nil_t &) const
    {
    }

    /*
    template<class T>
    void operator()(const T& i) const
    {
        //std::cout << i << std::endl;
    }
      */
private:
    void tab() const
    {
        for (auto i = 0; i < _tab; i++)
            std::cout << " ";
    }
    mutable int _tab;
};

void slot_msgpack(std::shared_ptr<const msgpack::type::variant> ptr)
{
    boost::apply_visitor(my_visitor(), *ptr);
}

void th_go()
{
    strip::utils::ioService::service.run();
    std::cout << "----------------------------------ioService exiting" << std::endl;
}

int sig_close_ext()
{
    std::cout << "CLOSE 3: external connections..." << std::endl;
    return 0;
}

int sig_close_ops()
{
    std::cout << "CLOSE 2: operations..." << std::endl;
    return 0;
}

int sig_close_db()
{
    std::cout << "CLOSE 1: database connections..." << std::endl;
    return 0;
}

int sig_close_log()
{
    std::cout << "CLOSE 0: log system..." << std::endl;
    return 0;
}

void read_log(std::shared_ptr<const strip::utils::LogMessage> msg)
{
    using namespace strip::utils;

    std::string lev;
    switch (msg->level)
    {
    case LogMessage::Level::INFO:
        lev = "II";
        break;
    case LogMessage::Level::DEBUG:
        lev = "DD";
        break;
    case LogMessage::Level::WARNING:
        lev = "WW";
        break;
    case LogMessage::Level::ERROR:
        lev = "EE";
        break;
    default:
        break;
    }

    std::cout << lev << "(" << msg->user << ") " << msg->emit_class << " :" << msg->message << std::endl;
}

constexpr int th_num = 20;

#include "include/strip/slo_control.hpp"
#include "include/strip/singleton.hpp"
#include <iomanip>
void check_addr()
{
    for (auto &a : strip::singletons.config->address_bias_pol)
        std::cout << a.name << " " << std::hex << "0x" << (int)a.address << " " << std::boolalpha << a.read_only << std::endl;

    std::cout << std::endl;
    for (auto &a : strip::singletons.config->address_bias_sys)
        std::cout << a.name << " " << std::hex << "0x" << (int)a.address << " " << std::boolalpha << a.read_only << std::endl;

    std::cout << std::endl;
    for (auto &a : strip::singletons.config->address_daq_pol)
        std::cout << a.name << " " << std::hex << "0x" << (int)a.address << " " << std::boolalpha << a.read_only << std::endl;

    std::cout << std::endl;
    for (auto &a : strip::singletons.config->address_daq_sys)
        std::cout << a.name << " " << std::hex << "0x" << (int)a.address << " " << std::boolalpha << a.read_only << std::endl;
}

#include <csignal>
#include <boost/process/child.hpp>
int main(int argc, char *argv[])
{
    namespace bp = boost::process;

    std::vector<std::thread> th;

    try
    {
        strip::utils::parse_config("/etc/strip/config.json");
        //strip::utils::parse_config("/home/stefano/workspace/LSPE/data-server/my_config.json");
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << "EXIT due to a configuration error" << std::endl;
        return -1;
    }

    int ret = strip::utils::init();
    if (ret != 0) //some error occurred while initializing sistem
        return ret;

    for (auto i = 0; i < th_num; i++) //start async workers for sockets
        th.push_back(std::thread(th_go));

    strip::utils::connect_signals();

    bp::child wamp_snapshot("./wamp_service");

    //auto conn_dbg = strip::singletons.cryo_stream->sig_cryo_pkt().connect(slot_msgpack); // DEBUG

    auto ch3 = strip::utils::UnixSignals::halt3_ext().connect(sig_close_ext);
    auto ch2 = strip::utils::UnixSignals::halt2_ops().connect(sig_close_ops);
    auto ch1 = strip::utils::UnixSignals::halt1_db().connect(sig_close_db);
    auto ch0 = strip::utils::UnixSignals::halt0_log().connect(sig_close_log);

    auto c1 = strip::utils::Log::sig_log().connect(read_log);
    strip::utils::Log::log(strip::utils::LogMessage::Level::INFO, "main", "begin data acquisition");

    for (auto &t : th)
        t.join();

    wamp_snapshot.terminate();

    return 0;
}
