#ifndef JSON_SLO_HPP
#define JSON_SLO_HPP

#include <boost/asio.hpp>
#include <memory>
#include "../redist/rapidjson/document.h"
#include "../redist/rapidjson/schema.h"

#include "../strip/slo_control.hpp"
#include "../strip/flags.hpp"
#include "../strip/tag.hpp"
#include "../strip/tcp.hpp"

namespace strip {


class JsonControl
{
public:
    static int init();

    std::string handle(const std::string &request);
    JsonControl();

    static const std::string class_name;
private:

    std::string send_status(SloCommand::Result status);
    std::string send_status(TagControl::Status status);

    std::string handle_slo(std::unique_ptr<rapidjson::Document> doc);
    std::string handle_cryo(std::unique_ptr<rapidjson::Document> doc);
    std::string handle_tag(std::unique_ptr<rapidjson::Document> doc);
    std::string handle_cmd(std::unique_ptr<rapidjson::Document> doc);
    std::string handle_log(std::unique_ptr<rapidjson::Document> doc);

    static std::unique_ptr<rapidjson::SchemaDocument> schema;
};

}
#endif // JSON_SLO_HPP
