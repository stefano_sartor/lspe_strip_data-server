#ifndef JSON_UTILS_HPP
#define JSON_UTILS_HPP

#define MSGPACK_USE_BOOST 1
#include <msgpack.hpp>
#include <string>
#include <sstream>
#include "../redist/rapidjson/rapidjson.h"
#include "../redist/rapidjson/document.h"

std::string serialize(const msgpack::type::variant& v);
msgpack::type::variant load(const rapidjson::Document& doc);

#endif