#ifndef AUTOBAHN_SNAPSHOT_WAMP_HPP
#define AUTOBAHN_SNAPSHOT_WAMP_HPP
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include "../strip/daq_pkt.hpp"
#include "../autobahn/base.hpp"
#include "../autobahn/snapshot.hpp"
#include <boost/variant.hpp>

namespace strip {
namespace autobahn{

class SnapshotWamp : public Base{
public:
    typedef Snapshot::value value;
    typedef Snapshot::value_pod value_pod;

    typedef Snapshot::addr_map addr_map;


    typedef Snapshot::snapshot_map snapshot_map;

    typedef Snapshot::arg_map arg_map;
    typedef Snapshot::result_map result_map;

    typedef Snapshot::sh_allocator sh_allocator;

    typedef Snapshot::sh_vector sh_vector;

    SnapshotWamp(const boost::asio::ip::address& addr,uint16_t port);

    boost::future<int> init(const std::string &realm);
    void leave();

    static const std::string class_name;
private:

    void snapshot(::autobahn::wamp_invocation invocation);

    snapshot_map _snapshot;

    boost::interprocess::managed_shared_memory _segment;
    std::unique_ptr<sh_vector> _sh_vec;

    ::autobahn::wamp_registration _registration;
};
}
}
#endif // STRIP_SNAPSHOT_HPP
