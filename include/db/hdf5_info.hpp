#ifndef DB_HDF5_INFO_HPP
#define DB_HDF5_INFO_HPP

#include "../strip/hdf5_stream.hpp"
#include <memory>
#include <odb/mysql/database.hxx>

namespace strip {

class DbH5info
{
public:
    DbH5info(const std::string &user, const std::string &password, const std::string &db_name, const std::string& host, uint16_t port);
    void connect_h5info(signal_hdf5_file& sig);

    static const std::string class_name;
private:
    H5FileInfo slot_h5info(std::shared_ptr<const H5FileInfo>);
    std::unique_ptr<odb::core::database> _db;
    signal_hdf5_file::Connection _conn;
};

}
#endif // DB_HDF5_INFO_HPP
