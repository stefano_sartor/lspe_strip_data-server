#ifndef DB_H5_INFO_HPP
#define DB_H5_INFO_HPP

#include <string>
#include <cstdint>
#include <memory>

namespace strip {
#pragma db object table("strip_h5")
class  H5FileInfo{
public:
#pragma db id auto
    uint64_t db_id;
    std::string relative_path;
#pragma db null
    std::unique_ptr<double> mjd_start;
#pragma db null
    std::unique_ptr<double> mjd_stop;

#pragma db null
    std::unique_ptr<int64_t> board_unix_s_start;
#pragma db null
    std::unique_ptr<int64_t> board_unix_us_start;

#pragma db null
    std::unique_ptr<int64_t> board_unix_s_stop;
#pragma db null
    std::unique_ptr<int64_t> board_unix_us_stop;


    // R board
#pragma db null
    std::unique_ptr<double>   R_mjd_start;
#pragma db null
    std::unique_ptr<double>   R_mjd_stop;

    // O board
#pragma db null
    std::unique_ptr<double>   O_mjd_start;
#pragma db null
    std::unique_ptr<double>   O_mjd_stop;

    // Y board
#pragma db null
    std::unique_ptr<double>   Y_mjd_start;
#pragma db null
    std::unique_ptr<double>   Y_mjd_stop;

    // G board
#pragma db null
    std::unique_ptr<double>   G_mjd_start;
#pragma db null
    std::unique_ptr<double>   G_mjd_stop;


    // B board
#pragma db null
    std::unique_ptr<double>   B_mjd_start;
#pragma db null
    std::unique_ptr<double>   B_mjd_stop;

    // I board
#pragma db null
    std::unique_ptr<double>   I_mjd_start;
#pragma db null
    std::unique_ptr<double>   I_mjd_stop;
#pragma db null

    // V board
#pragma db null
    std::unique_ptr<double>   V_mjd_start;
#pragma db null
    std::unique_ptr<double>   V_mjd_stop;


    H5FileInfo() : db_id(0){}
    H5FileInfo(const H5FileInfo& i){
        db_id = i.db_id;
        relative_path = i.relative_path;
        if(i.mjd_start) mjd_start.reset(new double(*i.mjd_start));
        if(i.mjd_stop) mjd_stop.reset(new double(*i.mjd_stop));

        if(i.board_unix_s_start)  board_unix_s_start.reset(new int64_t(*i.board_unix_s_start));
        if(i.board_unix_us_start) board_unix_us_start.reset(new int64_t(*i.board_unix_us_start));
        if(i.board_unix_s_stop)   board_unix_s_stop.reset(new int64_t(*i.board_unix_s_stop));
        if(i.board_unix_us_stop)  board_unix_us_stop.reset(new int64_t(*i.board_unix_us_stop));


        // R board
        if(i.R_mjd_start) R_mjd_start.reset(new double(*i.R_mjd_start));
        if(i.R_mjd_stop) R_mjd_stop.reset(new double(*i.R_mjd_stop));

        // O board
        if(i.O_mjd_start) O_mjd_start.reset(new double(*i.O_mjd_start));
        if(i.O_mjd_stop) O_mjd_stop.reset(new double(*i.O_mjd_stop));

        // Y board
        if(i.Y_mjd_start) Y_mjd_start.reset(new double(*i.Y_mjd_start));
        if(i.Y_mjd_stop) Y_mjd_stop.reset(new double(*i.Y_mjd_stop));

        // G board
        if(i.G_mjd_start) G_mjd_start.reset(new double(*i.G_mjd_start));
        if(i.G_mjd_stop) G_mjd_stop.reset(new double(*i.G_mjd_stop));


        // B board
        if(i.B_mjd_start) B_mjd_start.reset(new double(*i.B_mjd_start));
        if(i.B_mjd_stop) B_mjd_stop.reset(new double(*i.B_mjd_stop));

        // I board
        if(i.I_mjd_start) I_mjd_start.reset(new double(*i.I_mjd_start));
        if(i.I_mjd_stop) I_mjd_stop.reset(new double(*i.I_mjd_stop));

        // V board
        if(i.V_mjd_start) V_mjd_start.reset(new double(*i.V_mjd_start));
        if(i.V_mjd_stop) V_mjd_stop.reset(new double(*i.V_mjd_stop));
    }
};

}
#endif // DB_H5_INFO_HPP
