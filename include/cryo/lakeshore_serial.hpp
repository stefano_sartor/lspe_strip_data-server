#ifndef LAKESHORE_SERIAL_HPP
#define LAKESHORE_SERIAL_HPP
#include "lakeshore.hpp"

class LakeshoreSERIAL : public Lakeshore
{
public:
    virtual void set_config(const boost::property_tree::ptree &pt);
    virtual void start(boost::system::error_code& ec){}
    virtual void stop(boost::system::error_code& ec){}

protected:
    virtual void do_send(const std::stringstream &msg, boost::system::error_code& ec);
    virtual std::stringstream do_receive(uint64_t ms_timeout, boost::system::error_code& ec);
};

#endif