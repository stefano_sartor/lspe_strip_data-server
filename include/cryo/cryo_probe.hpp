#pragma once
#include "../redist/boost/math/interpolators/pchip.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

class CryoProbe {
public:
  const std::string &id() { return _id; }
  const std::string &cryo_unit(){return _cryo_unit;}
  const std::string &channel() { return _channel; }
  const std::string &sw_pos() { return _sw_pos; }
  const std::string &calibration_filename() { return _calibration_filename; }
  typedef boost::math::interpolators::pchip<std::vector<double>> interpolator_t;
  CryoProbe(const ::std::string &id,
            const ::std::string &cryo_unit,
            const ::std::string &channel,
            const ::std::string &sw_pos,
            const ::std::string &calibration_filename)
      : _id(id), _cryo_unit(cryo_unit), _channel(channel), _sw_pos(sw_pos),
        _calibration_filename(calibration_filename) {}
  void init(); // generate interpolator, throws exception on error
  double calibrate(const double &val);

private:
  std::string _id;
  std::string _cryo_unit;
  std::string _channel;
  std::string _sw_pos;
  std::string _calibration_filename;
  std::unique_ptr<interpolator_t> _interp;
};