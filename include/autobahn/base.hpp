#ifndef AUTOBAHN_BASE_HPP
#define AUTOBAHN_BASE_HPP
#include "../redist/autobahn/autobahn.hpp"
#include <boost/asio.hpp>
#include <memory>
#include <mutex>
#include <iostream>

namespace strip{
namespace autobahn{

class Base
{
public:
    Base(const boost::asio::ip::address& addr,uint16_t port);

    boost::future<int> init(const std::string &realm);

    template<class List, class Map>
    void publish(const std::string& topic, const List& list, const Map& map);
    void leave();

protected:
    std::shared_ptr<::autobahn::wamp_session> _session;
    std::mutex _m;
private:
    boost::asio::ip::tcp::endpoint _raw_endpoint;
    std::shared_ptr<::autobahn::wamp_tcp_transport> _wamp;
};

template<class List, class Map>
void Base::publish(const std::string& topic, const List& list, const Map& map){
    std::unique_lock<std::mutex> l(_m);
    boost::future<void> pub = _session->publish(topic, list, map);
    try {
        pub.wait(); //wait the send to hold the lock...
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
}


}
}

#endif // AUTOBAHN_BASE_HPP
