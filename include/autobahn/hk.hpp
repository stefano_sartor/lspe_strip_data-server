#ifndef AUTOBAHN_HK_HPP
#define AUTOBAHN_HK_HPP
#include "base.hpp"
#include "../strip/hk_stream.hpp"

namespace strip{
namespace autobahn{

class HkStream : public Base{
public:
    HkStream(const boost::asio::ip::address& addr,uint16_t port);
    void connect_hk(signal_hk_pkt& sig);

    static const std::string class_name;
private:
    void slot_pkt(std::shared_ptr<const msgpack::type::variant>);
    int  slot_close();

    std::vector<signal_hk_pkt::Connection> _conn_hk;
    utils::signal_halt::Connection _conn_close;
};

}
}
#endif // AUTOBAHN_HK_HPP
