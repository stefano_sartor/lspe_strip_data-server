#ifndef AUTOBAHN_TAG_HPP
#define AUTOBAHN_TAG_HPP
#include "base.hpp"
#include "../strip/utils.hpp"
#include "../strip/tag.hpp"

namespace strip{
namespace autobahn{

class Tag : public Base{
public:
    Tag(const boost::asio::ip::address& addr,uint16_t port);
    void connect_tag(signal_tag& sig);

    static const std::string class_name;
private:
    void slot_tag(std::shared_ptr<const DbTag>);
    int  slot_close();

    signal_tag::Connection _conn_tag;
    utils::signal_halt::Connection _conn_close;
};

}
}

#endif // AUTOBAHN_TAG_HPP
