#ifndef AUTOBAHN_DAQ_PKT_HPP
#define AUTOBAHN_DAQ_PKT_HPP
#include "daq_pkt.hpp"
#include "base.hpp"
#include "../strip/daq_pkt.hpp"
#include "../strip/utils.hpp"
#include <vector>

namespace strip{
namespace autobahn{

class DaqPkt : public Base{
public:
    typedef std::list<addr_item>::const_iterator const_iterator;

    DaqPkt(const boost::asio::ip::address& addr,uint16_t port);
    void connect_pkt(signal_pkt& sig);

    static const std::string class_name;
private:
    void slot_pkt(std::shared_ptr<const DaqUdpPkt>);
    int  slot_close();

    std::vector<signal_pkt::Connection> _conn_pkt;
    utils::signal_halt::Connection _conn_close;
};

}
}

#endif // AUTOBAHN_DAQ_PKT_HPP
