#ifndef STRIP_TCP
#define STRIP_TCP

#include <string>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <list>
#include "../strip/utils.hpp"

namespace strip
{

template<class Handler>
class ConnectionSlaveTCP
    : public boost::enable_shared_from_this<ConnectionSlaveTCP<Handler>>
{
    static constexpr uint64_t BUFFER_SIZE = 2048;

public:
    using pointer = boost::shared_ptr<ConnectionSlaveTCP<Handler>>;

    static pointer create(boost::asio::io_service &io_service, Handler *rh);

    boost::asio::ip::tcp::socket &socket();
    bool ok() const;
    void start_receive();

private:
    ConnectionSlaveTCP(boost::asio::io_service &io_service, Handler *hptr);

    void handle_read(const boost::system::error_code &ec, size_t s);

    boost::asio::ip::tcp::socket _socket;
    char _buffer[BUFFER_SIZE];
    std::unique_ptr<Handler> _handler;
    bool _ok;
};

template <class Handler>
class ServerTCP
{
public:
    ServerTCP(uint16_t port);

private:
    void start_accept();
    int slot_close();

    void handle_accept(typename ConnectionSlaveTCP<Handler>::pointer new_connection, const boost::system::error_code &error);

    boost::asio::ip::tcp::acceptor _acceptor;
    std::list<typename ConnectionSlaveTCP<Handler>::pointer> _conns;
    utils::signal_halt::Connection _conn_close;
};

} // namespace strip

#include "tcp.ipp"
#endif