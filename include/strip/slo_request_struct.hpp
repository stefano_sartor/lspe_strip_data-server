#ifndef SLO_REQUEST_STRUCT_HPP
#define SLO_REQUEST_STRUCT_HPP
#include <cstdint>
#include <vector>
#include <string>
#include <chrono>

namespace strip {

typedef struct{
    enum class Type { NONE, BIAS, CRYO, DAQ};
    enum class Method {NONE, SET, GET};
    enum class Result {OK=0,ERROR=1,UNKNOWN_COMMAND,ERROR_RO,BAD_ADDRESS,BAD_TYPE,ERROR_SYNTAX,ERROR_COMM,ERROR_DECODE,ERROR_DATA,ERROR_TIMEOUT_GET,ERROR_TIMEOUT_SET};

    std::string board_name;
    uint8_t pol_no;
    Method method;
    Type type;
    std::string base_addr;
    std::vector<uint16_t> data;
    std::string user = "system";

    int counter;
    Result res = Result::OK;
    std::string message;
    std::chrono::system_clock::time_point timestamp;
}SloCommand;

}
#endif // SLO_REQUEST_STRUCT_HPP
