#ifndef SLO_CONTROL_HPP
#define SLO_CONTROL_HPP
#include "flags.hpp"
#include "utils.hpp"
#include "config.hpp"
#include "slo_request_struct.hpp"

#include <boost/asio.hpp>
#include <mutex>
#include <vector>
#include <memory>
#include <map>

namespace strip
{

    class HkStream;

    typedef Signal<void(std::shared_ptr<const SloCommand>)> signal_slo_req;
    typedef Signal<void(std::shared_ptr<const SloCommand>)> signal_slo_ack;

    class SloControl
    {
        static constexpr uint64_t BUFFER_SIZE = 4096;
        static constexpr uint16_t COUNTER_BASE = 36;

        static const char encode_counter[COUNTER_BASE];

        static const size_t MAX_TIMEOUTS = 5;

    public:
        static constexpr int BOARD_ADDR = 0xF;
        static constexpr int NULL_ADDR = 0xE;

        typedef std::pair<boost::asio::ip::address, uint16_t> endpoint_t;
        typedef std::vector<endpoint_t> vec_endpoint_t;

        SloControl(const std::string &name, const boost::asio::ip::address &addr, uint16_t port);

        boost::system::error_code send_command(SloCommand &req, uint16_t millis = 200, bool sig = true);

        signal_pol_flag &sig_flag() { return _sig_flag; }
        signal_slo_req &sig_req() { return _sig_req; }
        signal_slo_ack &sig_ack() { return _sig_ack; }

        void set_hk_stream(std::shared_ptr<HkStream> ptr) { _hk_stream = ptr; }

        static const std::string class_name;

    private:
        bool is_response(const SloCommand &req, const SloCommand &recv);
        boost::system::error_code do_send_command(SloCommand &req, uint16_t millis, bool sig);

        std::string encode(SloCommand &req);
        SloCommand decode(std::string);

        void emit_flag(SloCommand &);
        void clear_flag(SloCommand &);

        void start_receive(uint64_t millis);
        int slot_close();

        void handle_timeout(const boost::system::error_code &error);
        void handle_receive(const boost::system::error_code &error,
                            std::size_t bytes_transferred);

        boost::system::error_code finalize_command(boost::system::error_code, SloCommand &req, bool sig);

        std::string _board_name;
        int _counter;

        size_t _n_timeouts; // after MAX_TIMEOUTS we refresh the connection

        boost::asio::ip::udp::endpoint _remote_endpoint;
        boost::asio::ip::udp::socket _socket;
        utils::signal_halt::Connection _conn_close;

        signal_pol_flag _sig_flag;
        signal_slo_req _sig_req;
        signal_slo_ack _sig_ack;

        std::mutex _cmd_sync;

        boost::system::error_code _error;

        char _buffer[BUFFER_SIZE];

        std::shared_ptr<HkStream> _hk_stream;
    };

} // namespace strip

#endif // SLO_CONTROL_HPP
