#ifndef DAQ_PKT_HPP
#define DAQ_PKT_HPP

#include <ctime>
#include <vector>
#include <array>
#include <memory>
#include <stdexcept>
#include "signals.hpp"


#define USE_FAKE_GPS 1
namespace strip{


constexpr static uint32_t DAQ_PKT_LEN = 280;
constexpr static uint32_t DAQ_UDP_PKTS = 5;
constexpr static uint32_t DAQ_UDP_LEN  = (DAQ_PKT_LEN * DAQ_UDP_PKTS);


constexpr static uint32_t NUM_BOARDS = 7;
constexpr static uint32_t NUM_POLS_PER_BOARD = 8;



typedef struct {
    int32_t  dem;
    uint32_t tpw;
} Channel;

typedef std::array<Channel,32> channels_t;

class DaqPkt {
public:

  constexpr static size_t HEADER_D   = 0x000;
  constexpr static size_t HEADER_A   = 0x001;
  constexpr static size_t HEADER_Q   = 0x002;
  constexpr static size_t BOARD_ID   = 0x003;
  constexpr static size_t POSIX_TIME = 0x004;
  constexpr static size_t SCI_DATA_0 = 0x008;
  constexpr static size_t BRST_PLS   = 0x108;
  constexpr static size_t BRST_LAT   = 0x109;
  constexpr static size_t SEC_H      = 0x10A;
  constexpr static size_t PH_ID      = 0x10B;

  /*constexpr static size_t CENTS      = 256;
  constexpr static size_t PHASE_B    = 257;
  constexpr static size_t N_SAMPLES  = 258;
  constexpr static size_t BLANK_TIME = 259;
  */
 //constexpr static size_t PKT_SEQ = (DAQ_PKT_LEN - 1); // usefull only for temp_stream

  constexpr static uint8_t HK_TABLE_BOARD= 0x0F;



    typedef char* iterator;

    char         board_id;
    timeval timestamp; //UNIX timestamp
    uint8_t      phb;
    uint8_t      sec_h;
    uint8_t      brst_pls;
    uint8_t      brst_lat;
    
    DaqPkt() = default;

    bool decode(const iterator& begin,const iterator& end,timeval* ts_offset = nullptr);

    bool encode(const iterator& begin,const iterator& end) const;

    int32_t& dem_Q1(size_t pol){return dem(pol,0);}
    int32_t& dem_U1(size_t pol){return dem(pol,1);}
    int32_t& dem_U2(size_t pol){return dem(pol,2);}
    int32_t& dem_Q2(size_t pol){return dem(pol,3);}

    const int32_t& dem_Q1(size_t pol)const {return dem(pol,0);}
    const int32_t& dem_U1(size_t pol)const {return dem(pol,1);}
    const int32_t& dem_U2(size_t pol)const {return dem(pol,2);}
    const int32_t& dem_Q2(size_t pol)const {return dem(pol,3);}

    uint32_t& pwr_Q1(size_t pol){return tpw(pol,0);}
    uint32_t& pwr_U1(size_t pol){return tpw(pol,1);}
    uint32_t& pwr_U2(size_t pol){return tpw(pol,2);}
    uint32_t& pwr_Q2(size_t pol){return tpw(pol,3);}

    const uint32_t& pwr_Q1(size_t pol)const {return tpw(pol,0);}
    const uint32_t& pwr_U1(size_t pol)const {return tpw(pol,1);}
    const uint32_t& pwr_U2(size_t pol)const {return tpw(pol,2);}
    const uint32_t& pwr_Q2(size_t pol)const {return tpw(pol,3);}

private:
    channels_t channels;

    bool is_valid(iterator b,iterator e);

    void decode_times(iterator s);
    void decode_channels(iterator s);

    void encode_times(iterator s) const;
    void encode_channels(iterator s) const;

    int32_t&  dem(size_t pol, size_t off);
    const int32_t&  dem(size_t pol, size_t off)const;

    uint32_t& tpw(size_t pol, size_t off);
   const uint32_t& tpw(size_t pol, size_t off) const;
};

typedef struct {
    char buffer[DAQ_UDP_LEN];
    DaqPkt pkts[DAQ_UDP_PKTS];
} DaqUdpPkt;

typedef Signal<void(std::shared_ptr<const DaqUdpPkt>)> signal_pkt;


inline int32_t& DaqPkt::dem(size_t pol, size_t off){
    if(pol < NUM_POLS_PER_BOARD)
        return channels[pol*4+off].dem;
    else
        throw std::out_of_range("max polarimeter number exeeded");
}

inline const int32_t& DaqPkt::dem(size_t pol, size_t off)const{
    if(pol < NUM_POLS_PER_BOARD)
        return channels[pol*4+off].dem;
    else
        throw std::out_of_range("max polarimeter number exeeded");
}

inline uint32_t& DaqPkt::tpw(size_t pol, size_t off){
    if(pol < NUM_POLS_PER_BOARD)
        return channels[pol*4+off].tpw;
    else
        throw std::out_of_range("max polarimeter number exeeded");
}

inline const uint32_t& DaqPkt::tpw(size_t pol, size_t off)const{
    if(pol < NUM_POLS_PER_BOARD)
        return channels[pol*4+off].tpw;
    else
        throw std::out_of_range("max polarimeter number exeeded");
}

}


#endif // DAQ_PKT_HPP
