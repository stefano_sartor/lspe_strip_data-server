#ifndef FILE_STREAM_HPP
#define FILE_STREAM_HPP

#include "utils.hpp"
#include "flags.hpp"
#include "tag.hpp"
#include "timer.hpp"
#include "daq_pkt.hpp"
#include "hk_stream.hpp"
#include <vector>
#include <string>

namespace strip {
class FileStream
{
public:
    FileStream();
    virtual ~FileStream(){slot_close();}

    void base_path(const std::string& path);
    void create(const std::string& str_time);

    const std::string& pathname() const {return _fullpath;}
    const std::string& filename() const {return _filename;}

    void connect_pkt(signal_pkt& sig);
    void connect_hk(signal_hk_pkt& sig);
    void connect_pol_flag(signal_pol_flag& sig);
    void connect_tel_flag(signal_tel_flag& tel);
    void connect_tag(signal_tag& sig);
    void connect_flush(signal_timer& sig);
    void connect_change_file(signal_timer& sig);

protected:
    void do_create();
    void close_file();
    std::string relative_path();

    void slot_add(std::shared_ptr<const DaqUdpPkt>);
    void slot_hk(std::shared_ptr<const msgpack::type::variant>);
    void slot_pol_flag(std::shared_ptr<const PolarimeterFlag>);
    void slot_tel_flag(std::shared_ptr<const TelescopeFlag>);
    void slot_tag(std::shared_ptr<const DbTag> tag);
    void slot_flush();
    void slot_change_file();
    int  slot_close();

    virtual void imp_flush() = 0;
    virtual void imp_add(std::shared_ptr<const DaqUdpPkt>&) = 0;
    virtual void imp_hk(std::shared_ptr<const msgpack::type::variant>&) = 0;
    virtual void imp_pol_flag(std::shared_ptr<const PolarimeterFlag>&) = 0;
    virtual void imp_tel_flag(std::shared_ptr<const TelescopeFlag>&) = 0;
    virtual void imp_tag(std::shared_ptr<const DbTag>&) = 0;
    virtual bool imp_open(const std::string&) = 0;
    virtual void imp_close() = 0;


    std::recursive_mutex _m;
    std::string _base_path;
    std::string _str_time;

    std::string _filename;
    std::string _fullpath;


    std::vector<signal_pkt::Connection> _conn_pkt;
    std::vector<signal_hk_pkt::Connection> _conn_hk;
    std::vector<signal_pol_flag::Connection> _conn_pol_flag;
    std::vector<signal_tel_flag::Connection> _conn_tel_flag;
    signal_tag::Connection _conn_tag;
    signal_timer::Connection _conn_timer_flush;
    signal_timer::Connection _conn_timer_change_file;
    utils::signal_halt::Connection _conn_close;

    std::string class_name;
    std::string ext;
};

}
#endif // FILES_TREAM_HPP
