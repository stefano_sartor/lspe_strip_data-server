#ifndef STRIP_CONFIG_HPP
#define STRIP_CONFIG_HPP

#include <string>
#include <list>
#include <map>
#include <vector>
#include <boost/property_tree/ptree.hpp>

namespace strip {

typedef struct{
    char id;
    std::string name;
    std::vector<std::string> pols;

    std::string sci_ip;
    uint16_t    sci_port;

    std::string slo_ip;
    uint16_t    slo_port;
}DaqConfig;

typedef struct {
    typedef enum {u16,i16} Type;
    std::string name;
    uint8_t address;
    Type type;
    bool read_only;
} addr_item;

typedef struct {
    std::string id;
    std::string cryo_unit;
    std::string channel;
    std::string sw_pos;
    std::string calibration;
} cryo_probe;

typedef struct{
    size_t workers;

    std::string db_user;
    std::string db_password;
    std::string db_name;
    std::string db_ip;
    uint16_t    db_port;

    std::string wamp_router_ip;
    uint16_t    wamp_router_port;
    std::string wamp_realm;


    std::map<char,std::shared_ptr<DaqConfig>> daq_id;
    std::map<std::string,std::shared_ptr<DaqConfig>> daq_name;

    std::map<std::string,boost::property_tree::ptree> cryo;
    std::pair<std::string,uint16_t> gps;

    uint64_t timer_raw_flush;
    uint64_t timer_raw_change_file;
    std::string raw_path;
    std::string raw_base_path;

    uint64_t timer_hdf5_flush;
    uint64_t timer_hdf5_change_file;
    std::string hdf5_base_path;
    std::string hdf5_path;

    int64_t timer_hk_scan;
    int64_t timer_hk_set;

    int64_t timer_cryo_scan;
    uint32_t multiplexer_hysteresis;

    std::string json_ip;
    uint16_t json_slo_port;

    std::list<addr_item> address_bias_pol;
    std::list<addr_item> address_bias_sys;
    std::list<addr_item> address_daq_pol;
    std::list<addr_item> address_daq_sys;

    std::list<cryo_probe> cryo_config;

    std::map<std::string,std::list<addr_item>::const_iterator> map_bias_pol_str;
    std::map<std::string,std::list<addr_item>::const_iterator> map_bias_sys_str;
    std::map<std::string,std::list<addr_item>::const_iterator> map_daq_pol_str;
    std::map<std::string,std::list<addr_item>::const_iterator> map_daq_sys_str;

    std::map<uint8_t,std::list<addr_item>::const_iterator> map_bias_pol_u8;
    std::map<uint8_t,std::list<addr_item>::const_iterator> map_bias_sys_u8;
    std::map<uint8_t,std::list<addr_item>::const_iterator> map_daq_pol_u8;
    std::map<uint8_t,std::list<addr_item>::const_iterator> map_daq_sys_u8;

    std::map<std::string,std::map<std::string,std::list<cryo_probe>::const_iterator>> cryo_map;

    std::string json_schema_path_slo;
}Config;



}



#endif // STRIP_CONFIG_HPP
