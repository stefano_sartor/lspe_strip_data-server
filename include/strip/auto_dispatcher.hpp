#ifndef AUTO_DISPATCHER_HPP
#define AUTO_DISPATCHER_HPP

#include "utils.hpp"
#include "flags.hpp"
#include "tag.hpp"
#include "daq_pkt.hpp"
#include <deque>

namespace strip {

class AFpkt {
  public:
    AFpkt() = default;
    void connect_pkt(signal_pkt& sig);
protected:
    virtual void do_pkt(std::shared_ptr<const DaqUdpPkt>&) = 0;
    void slot_pkt(std::shared_ptr<const DaqUdpPkt>);
    std::vector<signal_pkt::Connection> _conn_pkt;
};

class AFpol_flag {
  public:
    AFpol_flag() = default;
    void connect_pol_flag(signal_pol_flag& sig);
protected:
    virtual void do_pol_flag(std::shared_ptr<const PolarimeterFlag>&) = 0;
    void slot_pol_flag(std::shared_ptr<const PolarimeterFlag>);
    std::vector<signal_pol_flag::Connection> _conn_pol_flag;
};


class AFtel_flag {
  public:
    AFtel_flag() = default;
    void connect_tel_flag(signal_tel_flag& tel);
protected:
    virtual void do_tel_flag(std::shared_ptr<const TelescopeFlag>&) = 0;
    void slot_tel_flag(std::shared_ptr<const TelescopeFlag>);
    std::vector<signal_tel_flag::Connection> _conn_tel_flag;
};

class AutoDispatcher
{
public:
    static const std::string class_name;
    AutoDispatcher();
    void connect_pkt(signal_pkt& sig);
    void connect_pol_flag(signal_pol_flag& sig);
    void connect_tel_flag(signal_tel_flag& tel);

    void addAFpkt(std::shared_ptr<AFpkt> f);
    void addAFpol_flag(std::shared_ptr<AFpol_flag> f);
    void addAFtel_flag(std::shared_ptr<AFtel_flag> f);
private:
    void slot_pkt(std::shared_ptr<const DaqUdpPkt>);
    void slot_pol_flag(std::shared_ptr<const PolarimeterFlag>);
    void slot_tel_flag(std::shared_ptr<const TelescopeFlag>);
    int  slot_close();

    std::vector<signal_pkt::Connection> _conn_pkt;
    std::vector<signal_pol_flag::Connection> _conn_pol_flag;
    std::vector<signal_tel_flag::Connection> _conn_tel_flag;

    utils::signal_halt::Connection _conn_close;

    std::deque<std::shared_ptr<AFpkt>>      _af_pkt;
    std::deque<std::shared_ptr<AFpol_flag>> _af_pol_flag;
    std::deque<std::shared_ptr<AFtel_flag>> _af_teg_flag;


    signal_pkt _pkt;
    signal_pol_flag _pol_flag;
    signal_tel_flag _tel_flag;
};
}
#endif // AUTO_DISPATCHER_HPP
