#ifndef DAQ_UDP_SERVER_HPP
#define DAQ_UDP_SERVER_HPP

#include <boost/asio.hpp>

#include <memory>
#include "daq_pkt.hpp"
#include "utils.hpp"

namespace  strip {

class DaqUdpServer{
public:
  DaqUdpServer(const boost::asio::ip::address& addr,uint16_t port);
  signal_pkt& sig_pkt(){return _pkt_sig;}

private:
  void start_receive();
  int slot_close();

  void handle_receive(const boost::system::error_code& error,
      std::size_t bytes_transferred);

  timeval _ts_offset;
  /* discard first _warmup_pkts in order to empty the network buffers 
   * and properly calculate the timestap offset
   */ 
  size_t  _warmup_pkts;

  boost::asio::ip::udp::socket _socket;
  boost::asio::ip::udp::endpoint _remote_endpoint;
  std::shared_ptr<DaqUdpPkt> _udp_pkt;
  signal_pkt _pkt_sig;

  utils::signal_halt::Connection _conn_close;
};


}

#endif // DAQ_UDP_SERVER_HPP
