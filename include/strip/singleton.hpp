#ifndef STRIP_SINGLETON_HPP
#define STRIP_SINGLETON_HPP
#include "utils.hpp"
#include "tag.hpp"
#include "signals.hpp"
#include "daq_udp_server.hpp"
#include "slo_control.hpp"
#include "timer.hpp"
#include "daq_file_stream.hpp"
#include "hdf5_stream.hpp"
#include "auto_dispatcher.hpp"
#include "hk_stream.hpp"
#include "tcp.hpp"
#include "../autobahn/daq_pkt.hpp"
#include "../autobahn/log.hpp"
#include "../autobahn/tag.hpp"
#include "../autobahn/hk.hpp"
#include "../autobahn/snapshot.hpp"
#include "../db/log.hpp"
#include "../db/slo_command.hpp"
#include "../db/hdf5_info.hpp"
#include "../json/dispatcher.hpp"
#include "../cryo/cryo_unit.hpp"
#include "../cryo/cryo_stream.hpp"
#include "../cryo/cryo_probe.hpp"
#include <memory>
#include <vector>
#include <map>

namespace strip {
typedef struct{
    std::shared_ptr<ThreadPool> workers_pool;
    std::unique_ptr<utils::UnixSignals> unix_sig;

    std::unique_ptr<DbLog> db_log;
    std::unique_ptr<autobahn::Log> wamp_log;

    std::map<std::string,std::unique_ptr<DaqUdpServer>> daq_udp_servers;
    std::map<std::string,std::unique_ptr<autobahn::DaqPkt>> wamp_daq_pkt;
    std::unique_ptr<autobahn::Snapshot> wamp_snapshot;
    std::map<std::string,std::unique_ptr<DaqFileStream>> daq_file_streams;
    std::map<std::string,std::shared_ptr<HkStream>> hk_streams;
    std::unique_ptr<autobahn::HkStream> wamp_hk_stream;

    std::unique_ptr<H5Stream> hdf5_stream_sci;
    std::unique_ptr<DbH5info> db_hdf5;

    std::map<std::string,std::unique_ptr<SloControl>> slo_boards;
    std::map<std::string,std::unique_ptr<CryoUnit>> cryo_units;
    std::map<std::string,std::map<std::string,std::unique_ptr<CryoProbe>>> cryo_probes;

    std::unique_ptr<CryoStream> cryo_stream;

    std::unique_ptr<ServerTCP<JsonControl>> json_server;
    std::unique_ptr<DbSloCommand> db_slo;

    std::unique_ptr<TagControl> tag_control;
    std::unique_ptr<autobahn::Tag> wamp_tag;

    std::unique_ptr<Timer> timer_raw_flush;
    std::unique_ptr<Timer> timer_raw_change_file;

    std::unique_ptr<Timer> timer_hdf5_flush;
    std::unique_ptr<Timer> timer_hdf5_change_file;

    std::unique_ptr<Timer> timer_hk_scan;
    std::unique_ptr<Timer> timer_hk_set;

    std::unique_ptr<Timer> timer_cryo_scan;

    std::unique_ptr<AutoDispatcher> af_dispatcher;

    void reset_all(){
        daq_udp_servers.clear();
        daq_file_streams.clear();
        wamp_daq_pkt.clear();
        wamp_snapshot.reset();

        hk_streams.clear();

        timer_raw_flush.reset();
        timer_raw_change_file.reset();
        timer_hdf5_flush.reset();
        timer_hdf5_change_file.reset();
        timer_hk_scan.reset();
        timer_hk_set.reset();

        hdf5_stream_sci.reset();
        db_hdf5.reset();

        slo_boards.clear();
        cryo_units.clear();
        json_server.reset();
        db_slo.reset();

        tag_control.reset();
        wamp_tag.reset();

        db_log.reset();
        wamp_log.reset();
        wamp_hk_stream.reset();

        workers_pool.reset();

        af_dispatcher.reset();

        config.reset();
    }

    std::unique_ptr<Config> config;

}Singletons;

extern Singletons singletons;

}
#endif // STRIP_SINGLETON_HPP
