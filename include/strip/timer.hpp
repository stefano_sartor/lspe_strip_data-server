#ifndef TIMER_HPP
#define TIMER_HPP

#include <iostream>
#include "signals.hpp"
#include "utils.hpp"
#include <boost/asio.hpp>

namespace strip {

typedef Signal<void()> signal_timer;

class Timer
{
public:
    Timer();
    void start(uint64_t mills);
    void force_timeout();
    void stop();
    signal_timer& sig_timer(){return _sig;}

private:
    void emit_signal(const boost::system::error_code&);
    int slot_close();

    boost::asio::deadline_timer _timer;
    signal_timer _sig;
    uint64_t _ms;
    bool _go;

    utils::signal_halt::Connection _conn_close;
};

}
#endif // TIMER_HPP
