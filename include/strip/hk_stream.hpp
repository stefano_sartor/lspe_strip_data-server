#ifndef HK_STREAM_HPP
#define HK_STREAM_HPP

#define MSGPACK_USE_BOOST 1
#include <boost/variant.hpp>
#include <msgpack.hpp>
#include "signals.hpp"
#include "timer.hpp"
#include "slo_control.hpp"

namespace  strip {

typedef Signal<void(std::shared_ptr<const msgpack::type::variant>)> signal_hk_pkt;

class HkStream
{
public:
    HkStream(std::string board);
    void connect_hk(signal_timer& sig);
    void connect_set(signal_timer& sig);
    signal_hk_pkt& sig_hk_pkt(){return _hk_pkt_sig;}
    void send_sig(SloCommand& cmd);
    void read_hk(std::chrono::system_clock::time_point ts,uint16_t& value,size_t attempts=10);
private:
    void slot_collect_hk();
    void slot_collect_set();
    signal_hk_pkt _hk_pkt_sig;
    std::string _board;

    signal_timer::Connection _conn_timer_hk;
    signal_timer::Connection _conn_timer_set;
};
}
#endif // HK_STREAM_HPP
